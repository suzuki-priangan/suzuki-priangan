-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Apr 2019 pada 02.38
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suzuk165_suzuki_priangan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `acc`
--

CREATE TABLE `acc` (
  `id_acc` int(5) NOT NULL,
  `nama_acc` varchar(50) NOT NULL,
  `img_acc` text NOT NULL,
  `jum_acc` int(5) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aksesoris`
--

CREATE TABLE `aksesoris` (
  `id_aksesoris` int(12) NOT NULL,
  `aksesoris` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aksesoris`
--

INSERT INTO `aksesoris` (`id_aksesoris`, `aksesoris`) VALUES
(1, 'Ban Mobil'),
(2, 'Sok Mobil');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat`
--

CREATE TABLE `alamat` (
  `id_alamat` int(12) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL,
  `alamat_penerima` varchar(200) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `nama_kabupaten` varchar(50) NOT NULL,
  `kode_pos` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(5) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `img_artikel` text NOT NULL,
  `penulis` varchar(30) NOT NULL,
  `tgl_tulis` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `booking_service`
--

CREATE TABLE `booking_service` (
  `id_bs` int(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `wa` varchar(12) NOT NULL,
  `merk` varchar(30) NOT NULL,
  `tipe` varchar(10) NOT NULL,
  `no_vin` varchar(20) NOT NULL,
  `nopol` varchar(10) NOT NULL,
  `nama_stnk` varchar(50) NOT NULL,
  `layanan` varchar(30) NOT NULL,
  `wilayah` varchar(30) NOT NULL,
  `dealer` varchar(50) NOT NULL,
  `tgl` date NOT NULL,
  `jam_service` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detailkredit`
--

CREATE TABLE `detailkredit` (
  `id_detailKredit` int(5) NOT NULL,
  `tgl_kredit` date NOT NULL,
  `id_konsumen` int(5) NOT NULL,
  `id_mobil` int(5) NOT NULL,
  `id_jenis_pemesanan` int(5) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kode_referal` varchar(10) NOT NULL,
  `id_status` int(5) NOT NULL,
  `batas_akhir` date NOT NULL,
  `id_kredit` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_acc`
--

CREATE TABLE `detail_acc` (
  `id_detailacc` int(5) NOT NULL,
  `produk_kode` varchar(50) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `img_produk` text NOT NULL,
  `jum_produk` int(5) NOT NULL,
  `id_type_produk` int(5) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dp`
--

CREATE TABLE `dp` (
  `id_dp` int(5) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `tenor` varchar(50) NOT NULL,
  `total_dp` varchar(50) NOT NULL,
  `ansuran` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dp`
--

INSERT INTO `dp` (`id_dp`, `merk_mobil`, `tenor`, `total_dp`, `ansuran`) VALUES
(10, 'IGNIS GX MT', '36', '35,800,000', '5,910,000'),
(9, 'IGNIS GX MT', '24', '35,800,000', '7,880,000'),
(8, 'IGNIS GX MT', '12', '35,800,000', '14,011,000'),
(11, 'IGNIS GX MT', '48', '35,800,000', '4,979,000'),
(12, 'IGNIS GX MT', '60', '40,280,000', '4,411,000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE `galeri` (
  `id_galeri` int(5) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `img` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id_kabupaten` int(12) NOT NULL,
  `kabupaten` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`id_kabupaten`, `kabupaten`) VALUES
(1, 'Lombok Utara'),
(2, 'Lombok Barat'),
(3, 'Mataram'),
(4, 'Lombok Tengah'),
(5, 'Lombok Timur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kantor`
--

CREATE TABLE `kantor` (
  `id_kantor` int(12) NOT NULL,
  `nama_kantor` varchar(40) NOT NULL,
  `jenis_kantor` varchar(40) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(12) NOT NULL,
  `kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `kecamatan`) VALUES
(1, 'Kayangan'),
(2, 'Gangga'),
(4, 'Bayan'),
(5, 'Tanjung'),
(6, 'Pemenang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kelurahan` int(12) NOT NULL,
  `kelurahan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `kelurahan`) VALUES
(1, 'Dangiang'),
(2, 'Kayangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsumen`
--

CREATE TABLE `konsumen` (
  `id_konsumen` int(11) NOT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `tempat` varchar(30) NOT NULL,
  `tgl_lahir` varchar(25) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `siup` varchar(20) NOT NULL,
  `kode_referal` varchar(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsumen`
--

INSERT INTO `konsumen` (`id_konsumen`, `nama_konsumen`, `tempat`, `tgl_lahir`, `phone_number`, `email`, `siup`, `kode_referal`, `username`, `password`) VALUES
(13, 'Ivan Hadi', 'Lombok Utara', '21 Februari 1995', '082340088644', 'ivan23v1@gmail.com', '-', '12345', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kredit`
--

CREATE TABLE `kredit` (
  `id_kredit` int(12) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `nama_lokasi` varchar(50) NOT NULL,
  `tenor` varchar(50) NOT NULL,
  `total_dp` varchar(50) NOT NULL,
  `ansuran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kredit`
--

INSERT INTO `kredit` (`id_kredit`, `merk_mobil`, `nama_lokasi`, `tenor`, `total_dp`, `ansuran`) VALUES
(1, 'IGNIS GX MT', 'Bandung FC.', '5000000', '5000000', '5000000'),
(2, 'IGNIS GX MT', 'Bandung FC.', '4000000', '4000000', '4000000'),
(3, 'IGNIS GX MT', 'Bandung FC.', '12', '35,800,000', '14,011,000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kredit_perorangan`
--

CREATE TABLE `kredit_perorangan` (
  `id_kp` int(5) NOT NULL,
  `id_konsumen` int(5) NOT NULL,
  `id_alamat` int(5) NOT NULL,
  `jenis_identitas` varchar(20) NOT NULL,
  `no_identitas` varchar(30) NOT NULL,
  `pekerjaan` varchar(40) NOT NULL,
  `alamat_ktp` varchar(50) NOT NULL,
  `alamat_domisili` varchar(50) NOT NULL,
  `faktur_pajak` varchar(45) NOT NULL,
  `id_mobil` int(5) NOT NULL,
  `id_stok` int(5) NOT NULL,
  `id_tipemobil` int(5) NOT NULL,
  `harga_jadi` varchar(50) NOT NULL,
  `terbilang` varchar(50) NOT NULL,
  `tgl_pesan` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kredit_perusahaan`
--

CREATE TABLE `kredit_perusahaan` (
  `id_kredit_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `bentuk_usaha` varchar(50) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL,
  `no_aktependirian` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `pengajuan_pihaklain` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mobil`
--

CREATE TABLE `mobil` (
  `id_mobil` int(12) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `tipe` varchar(20) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `thn_produksi` varchar(50) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `img` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mobil`
--

INSERT INTO `mobil` (`id_mobil`, `merk_mobil`, `tipe`, `harga`, `thn_produksi`, `kategori`, `img`) VALUES
(37, 'IGNIS GX MT', 'IGNIS GX MT', '125000000', '2018', 'Mewah', '48d6215903dff56238e52e8891380c8f.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(12) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `jenis_pemesanan` varchar(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `batas_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penerima_kuasa`
--

CREATE TABLE `penerima_kuasa` (
  `id_penerima_kuasa` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `jenis_id` varchar(25) NOT NULL,
  `no_id` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengiriman`
--

CREATE TABLE `pengiriman` (
  `id_pengirim` int(12) NOT NULL,
  `tgl_pengiriman` date NOT NULL,
  `jasa_pengiriman` varchar(20) NOT NULL,
  `no_faktur` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `id_penjualan` int(12) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `pekerjaan` varchar(25) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perawatan`
--

CREATE TABLE `perawatan` (
  `id_perawatan` int(12) NOT NULL,
  `merk` varchar(30) NOT NULL,
  `kegunaan` varchar(100) NOT NULL,
  `img` text NOT NULL,
  `jumlah` int(5) NOT NULL,
  `harga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan_aksesoris`
--

CREATE TABLE `pesanan_aksesoris` (
  `id_pesanan` int(12) NOT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `aksesoris` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(12) NOT NULL,
  `provinsi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `provinsi`) VALUES
(1, 'Nusa Tenggara Barat'),
(2, 'Nusa Tenggara Timur'),
(3, 'Aceh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sparepart`
--

CREATE TABLE `sparepart` (
  `id_sparepart` int(12) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `kegunaan` varchar(50) NOT NULL,
  `img` text NOT NULL,
  `jumlah` int(5) NOT NULL,
  `harga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `spesifikasi`
--

CREATE TABLE `spesifikasi` (
  `id_spesifikasi` int(5) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `kekuatan` varchar(20) NOT NULL,
  `cc` varchar(15) NOT NULL,
  `pembakaran` varchar(50) NOT NULL,
  `transisi` varchar(50) NOT NULL,
  `torsi` varchar(45) NOT NULL,
  `tipe` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sponsor`
--

CREATE TABLE `sponsor` (
  `id_sponsor` int(12) NOT NULL,
  `nama_sponsor` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(11) NOT NULL,
  `merk_mobil` varchar(50) NOT NULL,
  `warna` varchar(30) NOT NULL,
  `indent` int(5) NOT NULL,
  `jml_nik` int(10) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `img` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimoni`
--

CREATE TABLE `testimoni` (
  `id_testimoni` int(12) NOT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `gambar` text NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipemobil`
--

CREATE TABLE `tipemobil` (
  `id_tipemobil` int(5) NOT NULL,
  `tipe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipemobil`
--

INSERT INTO `tipemobil` (`id_tipemobil`, `tipe`) VALUES
(41, 'BALENO MT'),
(42, 'BALENO AT'),
(49, 'MEGA CARRY EXTRA'),
(51, 'MEGA CARRY FD AC PS'),
(53, 'MEGA CARRY EXTRA AC PS'),
(55, 'MEGA CARRY FD'),
(61, 'KARIMUN WAGON R GS AB'),
(62, 'KARIMUN WAGON R GL AGS AB'),
(68, 'PU I,5 FD'),
(69, 'KARIMUN WAGON R GL AB'),
(70, 'KARIMUN WAGON R GS AGS AB'),
(71, 'IGNIS GL MT'),
(72, 'IGNIS GL AT'),
(73, 'IGNIS GX MT'),
(74, 'IGNIS GX AT'),
(75, 'ALL NEW ERTIGA GA'),
(76, 'ALL NEW ERTIGA MC GL MT'),
(77, 'ALL NEW ERTIGA MC GL AT'),
(78, 'ALL NEW ERTIGA MC GX MT'),
(79, 'ALL NEW ERTIGA MC GX AT'),
(80, 'APV GE PS MT AB'),
(81, 'APV GL MT AB'),
(82, 'APV GX MT AB'),
(83, 'APV SGX MT AB'),
(84, 'APV LUXURY R15 MT AB'),
(85, 'APV LUXURY R17 MT AB'),
(86, 'NEW SX-4 CROSS MT'),
(87, 'NEW SX-4 CROSS AT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tunai_perorangan`
--

CREATE TABLE `tunai_perorangan` (
  `id_tp` int(5) NOT NULL,
  `id_konsumen` int(5) NOT NULL,
  `id_alamat` int(5) NOT NULL,
  `jenis_identitas` varchar(20) NOT NULL,
  `no_identitas` varchar(30) NOT NULL,
  `pekerjaan` varchar(40) NOT NULL,
  `alamat_ktp` varchar(50) NOT NULL,
  `alamat_domisili` varchar(50) NOT NULL,
  `faktur_pajak` varchar(45) NOT NULL,
  `id_mobil` int(5) NOT NULL,
  `id_stok` int(5) NOT NULL,
  `id_tipemobil` int(5) NOT NULL,
  `harga_jadi` varchar(50) NOT NULL,
  `terbilang` varchar(50) NOT NULL,
  `tgl_pesan` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tunai_perusahaan`
--

CREATE TABLE `tunai_perusahaan` (
  `id_tunai_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `bentuk_usaha` varchar(50) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL,
  `no_aktependirian` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `pengajuan_pihaklain` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(12) NOT NULL,
  `username` varchar(999) NOT NULL,
  `password` varchar(999) NOT NULL,
  `nama` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`) VALUES
(1, 'admin', 'admin', 'Ivan Hadi');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `acc`
--
ALTER TABLE `acc`
  ADD PRIMARY KEY (`id_acc`);

--
-- Indeks untuk tabel `aksesoris`
--
ALTER TABLE `aksesoris`
  ADD PRIMARY KEY (`id_aksesoris`);

--
-- Indeks untuk tabel `alamat`
--
ALTER TABLE `alamat`
  ADD PRIMARY KEY (`id_alamat`);

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indeks untuk tabel `booking_service`
--
ALTER TABLE `booking_service`
  ADD PRIMARY KEY (`id_bs`);

--
-- Indeks untuk tabel `detail_acc`
--
ALTER TABLE `detail_acc`
  ADD PRIMARY KEY (`id_detailacc`),
  ADD KEY `id_type_produk` (`id_type_produk`);

--
-- Indeks untuk tabel `dp`
--
ALTER TABLE `dp`
  ADD PRIMARY KEY (`id_dp`);

--
-- Indeks untuk tabel `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indeks untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id_kabupaten`);

--
-- Indeks untuk tabel `kantor`
--
ALTER TABLE `kantor`
  ADD PRIMARY KEY (`id_kantor`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indeks untuk tabel `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`id_konsumen`);

--
-- Indeks untuk tabel `kredit`
--
ALTER TABLE `kredit`
  ADD PRIMARY KEY (`id_kredit`);

--
-- Indeks untuk tabel `kredit_perorangan`
--
ALTER TABLE `kredit_perorangan`
  ADD PRIMARY KEY (`id_kp`),
  ADD KEY `id_konsumen` (`id_konsumen`),
  ADD KEY `id_mobil` (`id_mobil`),
  ADD KEY `id_tipemobil` (`id_tipemobil`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indeks untuk tabel `kredit_perusahaan`
--
ALTER TABLE `kredit_perusahaan`
  ADD PRIMARY KEY (`id_kredit_perusahaan`);

--
-- Indeks untuk tabel `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`id_mobil`);

--
-- Indeks untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indeks untuk tabel `penerima_kuasa`
--
ALTER TABLE `penerima_kuasa`
  ADD PRIMARY KEY (`id_penerima_kuasa`);

--
-- Indeks untuk tabel `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`id_pengirim`);

--
-- Indeks untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indeks untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  ADD PRIMARY KEY (`id_perawatan`);

--
-- Indeks untuk tabel `pesanan_aksesoris`
--
ALTER TABLE `pesanan_aksesoris`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indeks untuk tabel `sparepart`
--
ALTER TABLE `sparepart`
  ADD PRIMARY KEY (`id_sparepart`);

--
-- Indeks untuk tabel `spesifikasi`
--
ALTER TABLE `spesifikasi`
  ADD PRIMARY KEY (`id_spesifikasi`);

--
-- Indeks untuk tabel `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`id_sponsor`);

--
-- Indeks untuk tabel `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indeks untuk tabel `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id_testimoni`);

--
-- Indeks untuk tabel `tipemobil`
--
ALTER TABLE `tipemobil`
  ADD PRIMARY KEY (`id_tipemobil`);

--
-- Indeks untuk tabel `tunai_perorangan`
--
ALTER TABLE `tunai_perorangan`
  ADD PRIMARY KEY (`id_tp`),
  ADD KEY `id_konsumen` (`id_konsumen`),
  ADD KEY `id_mobil` (`id_mobil`),
  ADD KEY `id_tipemobil` (`id_tipemobil`),
  ADD KEY `id_stok` (`id_stok`);

--
-- Indeks untuk tabel `tunai_perusahaan`
--
ALTER TABLE `tunai_perusahaan`
  ADD PRIMARY KEY (`id_tunai_perusahaan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `acc`
--
ALTER TABLE `acc`
  MODIFY `id_acc` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `aksesoris`
--
ALTER TABLE `aksesoris`
  MODIFY `id_aksesoris` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `alamat`
--
ALTER TABLE `alamat`
  MODIFY `id_alamat` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `booking_service`
--
ALTER TABLE `booking_service`
  MODIFY `id_bs` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `dp`
--
ALTER TABLE `dp`
  MODIFY `id_dp` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_galeri` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id_kabupaten` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kantor`
--
ALTER TABLE `kantor`
  MODIFY `id_kantor` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_kelurahan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `kredit`
--
ALTER TABLE `kredit`
  MODIFY `id_kredit` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kredit_perorangan`
--
ALTER TABLE `kredit_perorangan`
  MODIFY `id_kp` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kredit_perusahaan`
--
ALTER TABLE `kredit_perusahaan`
  MODIFY `id_kredit_perusahaan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mobil`
--
ALTER TABLE `mobil`
  MODIFY `id_mobil` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `penerima_kuasa`
--
ALTER TABLE `penerima_kuasa`
  MODIFY `id_penerima_kuasa` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengiriman`
--
ALTER TABLE `pengiriman`
  MODIFY `id_pengirim` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_penjualan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  MODIFY `id_perawatan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pesanan_aksesoris`
--
ALTER TABLE `pesanan_aksesoris`
  MODIFY `id_pesanan` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sparepart`
--
ALTER TABLE `sparepart`
  MODIFY `id_sparepart` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `spesifikasi`
--
ALTER TABLE `spesifikasi`
  MODIFY `id_spesifikasi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sponsor`
--
ALTER TABLE `sponsor`
  MODIFY `id_sponsor` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id_testimoni` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tipemobil`
--
ALTER TABLE `tipemobil`
  MODIFY `id_tipemobil` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT untuk tabel `tunai_perorangan`
--
ALTER TABLE `tunai_perorangan`
  MODIFY `id_tp` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tunai_perusahaan`
--
ALTER TABLE `tunai_perusahaan`
  MODIFY `id_tunai_perusahaan` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
