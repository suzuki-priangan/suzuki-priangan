        <header id="header" class="htc-header header--3 bg__white">
            <!-- Start Mainmenu Area -->
            <div id="sticky-header-with-topbar" class="mainmenu__area sticky__header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <div class="logo">
                                <a href="?tampil=beranda">
                                    <img src="images/logo/logo2.png" alt="logo">
                                </a>
                            </div>
                        </div>
                        <!-- Start MAinmenu Ares -->
                        <div class="col-md-8 col-lg-8 col-sm-6 col-xs-6">
                            <nav class="mainmenu__nav hidden-xs hidden-sm">
                                <ul class="main__menu">
                                    <li class="drop"><a href="?tampil=beranda">Beranda</a></li>
                                    <li class="drop"><a href="?tampil=mobil">Garasi</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=mobil">Mobil</a></li>
                                            <li><a href="?tampil=aksesoris_kategori">Aksesoris</a></li>
                                            <li><a href="?tampil=karoseri">Karoseri</a></li>
                                        </ul>
                                    </li>
                                    <li class="drop"><a href="?tampil=service">Bengkel</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=service">Booking Service</a></li>
                                        </ul>
                                    </li>
                                    <li class="drop"><a href="?tampil=artikel">Corner</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=artikel">Artikel</a></li>
                                            <li><a href="?tampil=tips_trik">Tips & Trik</a></li>
                                        </ul>
                                    </li>
                                    <li class="drop"><a href="?tampil=promo">Bazar</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=promo">Promo</a></li>
                                            <li><a href="?tampil=event">Event</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="?tampil=forum">Forum</a></li>
                                </ul>
                            </nav>
                            <div class="mobile-menu clearfix visible-xs visible-sm">
                                <nav id="mobile_dropdown">
                                    <ul>
                                        <li><a href="?tampil=beranda">Beranda</a></li>
                                        <li><a href="?tampil=mobil">Garasi</a>
                                            <ul>
                                                <li><a href="?tampil=mobil">Mobil</a></li>
                                                <li><a href="?tampil=aksesoris_kategori">Aksesoris</a></li>
                                                <li><a href="?tampil=karoseri">Karoseri</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="?tampil=service">Bengkel</a>
                                            <ul>
                                                <li><a href="?tampil=service">Pesan Servis</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="?tampil=artikel">Pojok</a>
                                            <ul>
                                                <li><a href="?tampil=artikel">Artikel</a></li>
                                                <li><a href="?tampil=tips_trik">Tips & Trik</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="?tampil=promo">Bazar</a>
                                            <ul>
                                                <li><a href="?tampil=promo">Promo</a></li>
                                                <li><a href="?tampil=event">Acara</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="?tampil=promo">Forum</a></li>
                                    </ul>
                                </nav>
                            </div>                          
                        </div>
                        <!-- End MAinmenu Ares -->
                        <div class="col-md-2 col-sm-4 col-xs-3">  
                            <ul class="menu-extra">
                                <li class="search search__open hidden-xs" title="Search"><span class="ti-search"></span></li>
                                <?php
                                if (!defined ("INDEX")) {
                                ?>
                                <li><a href="?tampil=login_form" title="Login / Registrasi"><span class="ti-user"></span></a></li>
                                <li class="cart__menu" title="Keranjang"><span class="ti-shopping-cart"></span></li>
                                <?php
                                } else {
                                ?>
                                <li><a href="?tampil=keranjang" title="Keranjang"><span class="ti-shopping-cart"><?php
                                $kuery = "SELECT count(*) as jumlah FROM keranjang where id_konsumen='$konsumen[id_konsumen]'";
                                $cek = mysqli_query($koneksi, $kuery);
			                    $hasil = mysqli_fetch_array($cek);
			                    if ($hasil == null) {
			                        echo ''; 
			                    } else {
			                    echo "$hasil[jumlah]";
			                    }
			                    ?></span></a></li>
                                <li><a href="?tampil=profil" title="Profil"><span class="ti-user"></span></a></li>
                                <li><a href="?tampil=logout" title="Keluar"><span class="ti-power-off"></span></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-menu-area"></div>
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>