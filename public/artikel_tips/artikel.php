    <?php
        include ("lib/koneksi.php");
    ?>
    
    <section class="htc__product__area shop__page ptb--30 bg__white">
                <div class="container">
                    <div class="htc__product__container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section__title section__title--2 text-center">
                                    <h2 class="title__line">Artikel</h2>
                                </div>
                            </div>
                        </div>
                <div class="row">
                    <div class="blog__wrap clearfix mt--60 xmt-30">
                        <?php
                        
                        $batas = 120;
                        $sql = mysqli_query ($koneksi, "SELECT judul, isi, id_artikel, tgl_tulis, img_artikel FROM artikel order by tgl_tulis desc");
                        while ($data = mysqli_fetch_array ($sql)){

                        ?>
                        <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                    <div class="product foo atas">
                        <div class="product__inner">
                            <div class="pro__thumb">
                                <a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">
                                    <img src="img/artikel/<?php echo $data['img_artikel']; ?>" alt="blog images">
                                </a>
                            </div>
                        </div>
                        <div class="product__details">
                            <p class="text-left"><?php echo $data['tgl_tulis']; ?></p>
                            <h2 class="text-left"><a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>"><strong><?php echo $data['judul']; ?></strong></a></h2>
                            <p class="blog__des text-left"><?php echo substr($data['isi'], 0, $batas) . '...'; ?></p>
                            <br>
                            <p class="text-left"><a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">READ MORE.. </a></p>
                        </div>
                        <br>
                    </div>
                </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        </section>