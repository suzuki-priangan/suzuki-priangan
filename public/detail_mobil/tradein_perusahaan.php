<?php

    include ("lib/koneksi.php");

?>    
<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Pengajuan Trade In Perusahaan</h2>
                </div>
            </div>
        </div>
        <div class="checkout-form">
            <div class="checkout-form-inner">
                <form action="?tampil=cash_perusahaan_proses" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="JenisMobil">Jenis Mobil</label>
                            <input type="text" class="form-control" name="merk_mobil" id="JenisMobil" value="<?php echo $_POST['merk']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="TipeMobil">Tipe Mobil</label>
                            <input type="text" class="form-control" name="tipe" id="TipeMobil" value="<?php echo $_POST['tipe']; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Harga">Harga</label>
                            <input type="text" class="form-control" name="harga" id="Harga" value="<?php echo $_POST['harga']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Warna">Warna</label>
                            <select name="warna" class="form-control-sm form-control">
                                <?php
                                $q=mysqli_query($koneksi, "SELECT distinct warna FROM stok where    jenis_mobil='$_POST[merk]'");
                                while($warna=mysqli_fetch_array($q)){
            
                                ?>
                                <option value="<?php echo $warna["warna"] ?>">
                                    <?php echo $warna["warna"] ?></option>
                                             
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <p>Silahkan isi data Mobil Anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Nama Mobil</label>
                            <input type="text" class="form-control-sm form-control" name="nama_mobil" placeholder="Nama Mobil (Contoh = Suzuki Ignis)" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tipe Mobil</label>
                            <input type="text" class="form-control-sm form-control" name="tipe_mobil" placeholder="Tipe Mobil" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tahun</label>
                            <input type="text" class="form-control-sm form-control" name="tahun" placeholder="Tahun" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Transmisi</label>
                            <input type="text" class="form-control-sm form-control" name="transmisi" placeholder="Transmisi" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Foto Eksterior Depan</label>
                            <input type="file" class="form-control-sm form-control" name="eksterior_depan" placeholder="Eksterior Depan" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Foto Eksterior Belakang</label>
                            <input type="file" class="form-control-sm form-control" name="eksterior_belakang" placeholder="Eksterior Belakang" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Foto Kilometer</label>
                            <input type="file" class="form-control-sm form-control" name="kilometer" placeholder="Kilometer" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Foto STNK</label>
                            <input type="file" class="form-control-sm form-control" name="stnk" placeholder="STNK" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <p>Silahkan isi data diri Anda. Kami hanya akan menanyakan data Anda sekali ini saja, dan data Anda aman bersama kami.  </p>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <input type="hidden" class="form-control" name="kabupaten" id="Kabupaten" value="<?php echo $_POST['kabupaten']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <input type="hidden" class="form-control" name="kecamatan" id="kecamatan" value="<?php echo $_POST['kecamatan']; ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="NamaPerusahaan">Nama Perusahaan</label>
                            <input type="text" class="form-control" name="nama_perusahaan" id="NamaPerusahaan" placeholder="Nama Perusahaan" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="BidangUsaha">Bidang Usaha</label>
                            <input type="text" class="form-control" name="bidang_usaha" id="BidangUsaha" placeholder="Bidang Usaha" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="NoAkta">No. Akta Pendirian</label>
                            <input type="text" class="form-control" name="no_akta" id="NoAkta" placeholder="No. Akta Pendirian" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="NoTlpn">No. Telepon</label>
                            <input type="text" class="form-control" name="no_tlpn" id="NoTlpn" placeholder="No. Telepon" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="Alamat">Email</label>
                            <input type="email" class="form-control" name="email" id="Alamat" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Bentuk Badan Usaha</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="bentuk_usaha" id="inlineRadio1" value="CV" required>
                                <label class="form-check-label" for="inlineCheckbox1">CV</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="bentuk_usaha" id="inlineRadio1" value="PT">
                                <label class="form-check-label" for="inlineCheckbox2">PT</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="bentuk_usaha" id="inlineRadio1" value="Yayasan">
                                <label class="form-check-label" for="inlineCheckbox2">Yayasan</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="bentuk_usaha" id="inlineRadio1" value="lain-lain">
                                <input type="text" class="form-control form-control-sm inline"
                                            id="colFormLabelSm" placeholder="Lain-Lain">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Apakah Transaksi diajukan untuk pihak lain?</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="tipe_transaksi" id="inlineRadio1" value="Transaksi diajukan untuk pihak lain" required>
                                <label class="form-check-label" for="inlineCheckbox1">Ya</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="tipe_transaksi" id="inlineRadio1" value="Transaksi diajukan bukan untuk pihak lain">
                                <label class="form-check-label" for="inlineCheckbox2">Tidak</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <div class="col-md-6">
                            <input type="checkbox" class="form-check-input" name="setuju" value="Saya akan menyiapkan persyaratan yang dibutuhkan" required>
                            <label class="form-check-label" for="exampleCheck1">Saya akan menyiapkan persyaratan yang dibutuhkan</label>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
    <section class="htc__blog__area bg__white pb--130">
        <div class="container">
    </div>
</section>