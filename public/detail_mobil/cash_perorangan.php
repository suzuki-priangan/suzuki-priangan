<?php

    include ("lib/koneksi.php");

?>    
<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Pengajuan Cash Perorangan</h2>
                </div>
            </div>
        </div>
        <div class="checkout-form">
            <div class="checkout-form-inner">
                <form action="?tampil=cash_perorangan_proses" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="JenisMobil">Jenis Mobil</label>
                            <input type="text" class="form-control" name="merk_mobil" id="JenisMobil" value="<?php echo $_POST['merk']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="TipeMobil">Tipe Mobil</label>
                            <input type="text" class="form-control" name="tipe" id="TipeMobil" value="<?php echo $_POST['tipe']; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Harga">Harga</label>
                            <input type="text" class="form-control" name="harga" id="Harga" value="<?php echo $_POST['harga']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Warna">Warna</label>
                            <select name="warna" class="form-control-sm form-control">
                                <?php
                                $q=mysqli_query($koneksi, "SELECT distinct warna FROM stok where    jenis_mobil='$_POST[merk]'");
                                while($warna=mysqli_fetch_array($q)){
            
                                ?>
                                <option value="<?php echo $warna["warna"] ?>">
                                    <?php echo $warna["warna"] ?></option>
                                             
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <p>Silahkan isi data diri Anda. Kami hanya akan menanyakan data Anda sekali ini saja, dan data Anda aman bersama kami.  </p>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <input type="hidden" class="form-control" name="kabupaten" id="Kabupaten" value="<?php echo $_POST['kabupaten']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <input type="hidden" class="form-control" name="kecamatan" id="Kecamatan" value="<?php echo $_POST['kecamatan']; ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Nama">Nama</label>
                            <input type="text" class="form-control" name="nama" id="Nama" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="NoHp">No Hand Phone</label>
                            <input type="text" class="form-control" name="no_hp" id="NoHp" placeholder="No Hand Phone" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control" name="email" id="Email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <div class="col-md-12">
                            <input type="checkbox" class="form-check-input" name="ktp" value="Saya akan menyiapkan persyaratan yang dibutuhkan" required>
                            <label class="form-check-label" for="exampleCheck1">Saya akan menyiapkan persyaratan yang dibutuhkan</label>
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <div class="col-md-12">
                            <input type="checkbox" class="form-check-input" name="setuju" value="Saya akan menyiapkan persyaratan yang dibutuhkan" required>
                            <label class="form-check-label" for="exampleCheck1">Saya akan menyiapkan persyaratan yang dibutuhkan</label>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
    <section class="htc__blog__area bg__white pb--130">
        <div class="container">
    </div>
</section>