<?php

    include ("lib/koneksi.php");
    $tes = mysqli_query ($koneksi, "SELECT * FROM detail_mobil where id_detail_mobil='$_GET[id]'");
    $data2 = mysqli_fetch_array ($tes);

?> 
        <section class="htc__product__area shop__page ptb--50 bg__white">
            <div class="container">
                <div class="htc__product__container"><div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <h2 class="title__line"><?php echo $data2['jenis_mobil'];?></h2>
                                <br>
                            </div>
                        </div>
                    </div>
        <div class="row mb-5 mt-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <h6><a href="?tampil=beranda">Beranda</a> > <a href="?tampil=mobil">Mobil</a></h6>
                        <hr>
                        <div class="">
                            <!-- Slick Sync Slider For -->
                            <div class="slider-for" style="height:100%; width:100%;">
                                <!-- item 1 -->
                                <?php
                                    $q=mysqli_query($koneksi, "SELECT gambar from stok where jenis_mobil='$data2[jenis_mobil]'");
                                    while($img=mysqli_fetch_array($q)){
                                ?>

                                <div class="item">
                                    <div class="preview">
                                        <img class="img-responsive border border-secondary" 
                                            src="../../img/stok/<?php echo $img['gambar'];?>" 
                                            alt=""
                                            style="width:auto; height:auto;">
                                    </div>
                                </div>
                                <?php
                                    }
                                ?>

                            </div>
                        </div>

                        <!-- Slick Sync Slider Nav -->
                        <br>
                        <h6><i>Model</i></h6>
                        <hr>
                        <div class="slider-nav d-inline" style="height:auto; width:auto;">
                            <!-- item 1 -->
                            
                            <?php
                                $q=mysqli_query($koneksi, "SELECT gambar from stok where jenis_mobil='$data2[jenis_mobil]'");
                                while($img=mysqli_fetch_array($q)) {
                            ?>
                            
                            <div class="item" style="height:auto; width:auto;">
                                <img class="img-fluid img-thumbnail mr-1" 
                                    src="img/stok/<?php echo $img['gambar']; ?>" 
                                    alt="Responsive image"
                                    draggable="false" 
                                    style="width:auto; height:auto;" />
                            </div>
                            &nbsp;
                            
                            <?php
                                }
                            ?>
                            
                        </div>
                            <div class="mb-5">
                                <a href="img/spesifikasi/<?php echo $data2['spesifikasi'] ?>" class="btn btn-info"> E-Brosur</a>
                                &nbsp;
                                <a class="btn btn-info" href="<?php echo $data2['link'] ?>" target="_blank"> Spesifikasi</a>
                                
                            </div>
                        
                        
                        
                    </div>

                    <div class="col-md-4">
                        <div>
                            <h6>Ajukan Pembelian</h6>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="">

                                    <div class="accordion" id="accordionExample">

                                        <div class="formbayar">
                                            <div class="" id="headingOne">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-block btn-info btn-lg" type="button"
                                                        data-toggle="collapse" data-target="#collapseOne"
                                                        aria-expanded="true" aria-controls="collapseOne">

                                                        Ajukan Secara Kredit

                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                                data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <form method="post">
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control-sm form-control"
                                                            name="merk" id="merk"
                                                            value="<?php echo $data2['jenis_mobil']; ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Lokasi
                                                        </label>

                                                        <select name="kabupaten" id="kabupaten"
                                                            class="form-control-sm form-control">
                                                            //1
                                                            <option value="Kota Bandung">Kota Bandung</option>
                                                            <option value="Kabupaten Bandung">Kabupaten Bandung</option>
                                                            <option value="Kabupaten Bandung Barat">Kabupaten Bandung Barat</option>
                                                            <option value="Kota Cimahi">Kota Cimahi</option>
                                                            <!--
                                                            <option value="Kabupaten Tasikmalaya">Kabupaten Tasikmalaya</option>
                                                            <option value="Kota Tasikmalaya">Kota Tasikmalaya</option>
                                                            <option value="Kabupaten Sumedang">Kota Sumedang</option>
                                                            -->
                                                            //2
                                                            <option value="Kabupaten Majalengka">Kabupaten Majalengka</option>
                                                            <option value="Kabupaten Indramayu">Kabupaten Indramayu</option>
                                                            <option value="Kabupaten Kuningan">Kabupaten Kuningan</option>
                                                            <option value="Kabupaten Cirebon">Kabupaten Cirebon</option>
                                                            <option value="Kota Cirebon">Kota Cirebon</option>
                                                            //3
                                                            <option value="Kabupaten Subang">Kabupaten Subang</option>
                                                            <option value="Kabupaten Karawang">Kabupaten Karawang</option>
                                                            <option value="Kabupaten Purwakarta">Kabupaten Purwakarta</option>
                                                            //4
                                                            <option value="Kabupaten Garut">Kabupaten Garut</option>
                                                            <!--
                                                            <option value="Kabupaten Adm. Kep. Seribu">Kabupaten Adm. Kep. Seribu</option>
                                                            <option value="Kota Adm. Jakarta Pusat">Kota Adm. Jakarta Pusat</option>
                                                            <option value="Kota Adm. Jakarta Utara">Kota Adm. Jakarta Utara</option>
                                                            <option value="Kota Adm. Jakarta Barat">Kota Adm. Jakarta Barat</option>
                                                            <option value="Kota Adm. Jakarta Selatan">Kota Adm. Jakarta Selatan</option>
                                                            <option value="Kota Adm. Jakarta Timur">Kota Adm. Jakarta Timur</option>
                                                            <option value="Kota Tangerang">Kota Tangerang</option>
                                                            <option value="Kota Tangerang Selatan">Kota Tangerang Selatan</option>
                                                            <option value="Kabupaten Tangerang">Kabupaten Tangerang</option>
                                                            -->
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Kecamatan
                                                        </label>
                                                        <select class="form-control-sm form-control" name="kecamatan" id="kecamatan" required>
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT kecamatan FROM kecamatan where kabupaten='Kota Bandung'");
                                                            while($data_per=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_per["kecamatan"] ?>">
                                                                <?php echo $data_per["kecamatan"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Tipe Mobil
                                                        </label>

                                                        <select name="tipe" id="tipe"
                                                            class="form-control-sm form-control">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct tipe FROM detail_mobil where jenis_mobil='$data2[jenis_mobil]' order by id_detail_mobil='$data2[id_detail_mobil]' desc");
                                                            while($data_per=mysqli_fetch_array($q)){
        
                                                            ?>
                                                            <option value="<?php echo $data_per["tipe"] ?>">
                                                                <?php echo $data_per["tipe"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Harga
                                                        </label>

                                                        <select id="harga" name="harga"class="form-control-sm form-control">
                                                            <?php
                                                            $hrg = mysqli_query ($koneksi, "SELECT distinct harga FROM dp where id_detail_mobil='$data2[id_detail_mobil]'");
                                                            $harga = mysqli_fetch_array ($hrg);
                                                            ?>
                                                            <option value="<?php echo $harga["harga"] ?>">
                                                                <?php echo "Rp ".number_format($harga['harga'])?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            DP
                                                        </label>

                                                        <select id="dp" name="dp" class="form-control-sm form-control">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct total_dp FROM dp where id_mobil='$data2[id_detail_mobil]' order by tenor desc");
                                                            while($data_per=mysqli_fetch_array($q)){
        
                                                            ?>
                                                            <option value="<?php echo $data_per["total_dp"] ?>">
                                                                <?php echo "Rp ".number_format($data_per['total_dp']) ?>
                                                            </option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Angsuran
                                                        </label>

                                                        <select id="angsuran" name="angsuran"
                                                            class="form-control-sm form-control">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT tenor, angsuran FROM dp where id_mobil='$data2[id_detail_mobil]' order by tenor desc");
                                                            while($data_per=mysqli_fetch_array($q)){
        
                                                            ?>
                                                            <option value="<?php echo "Rp. ".number_format($data_per['angsuran']) ?> x <?php echo $data_per["tenor"] ?> Bulan"><?php echo $data_per["tenor"] ?> Bulan x <?php echo "Rp. ".number_format($data_per['angsuran']) ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=kredit_perorangan">
                                                            Perorangan
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=kredit_perusahaan">
                                                            Badan Usaha
                                                        </button>
                                                    </div>
                                                    </form>
                                                </div>


                                            </div>
                                        </div>
                                        <br>
                                        <div class="formbayar">
                                            <div class="" id="headingTwo">
                                                <h2 class="mb-0">

                                                    <button class="btn btn-block btn-info btn-lg " type="button"
                                                        data-toggle="collapse" data-target="#collapseTwo"
                                                        aria-expanded="true" aria-controls="collapseTwo">

                                                        Ajukan Secara Cash
                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                                data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <form method="post">
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control-sm form-control"
                                                            name="merk" id="2_merk"
                                                            value="<?php echo $data2['jenis_mobil']; ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Lokasi
                                                        </label>
                                                        <select name="kabupaten" id="kabupaten"
                                                            class="form-control-sm form-control">
                                                            //1
                                                            <option value="Kota Bandung">Kota Bandung</option>
                                                            <option value="Kabupaten Bandung">Kabupaten Bandung</option>
                                                            <option value="Kabupaten Bandung Barat">Kabupaten Bandung Barat</option>
                                                            <option value="Kota Cimahi">Kota Cimahi</option>
                                                            <!--
                                                            <option value="Kabupaten Tasikmalaya">Kabupaten Tasikmalaya</option>
                                                            <option value="Kota Tasikmalaya">Kota Tasikmalaya</option>
                                                            <option value="Kabupaten Sumedang">Kota Sumedang</option>
                                                            -->
                                                            //2
                                                            <option value="Kabupaten Majalengka">Kabupaten Majalengka</option>
                                                            <option value="Kabupaten Indramayu">Kabupaten Indramayu</option>
                                                            <option value="Kabupaten Kuningan">Kabupaten Kuningan</option>
                                                            <option value="Kabupaten Cirebon">Kabupaten Cirebon</option>
                                                            <option value="Kota Cirebon">Kota Cirebon</option>
                                                            //3
                                                            <option value="Kabupaten Subang">Kabupaten Subang</option>
                                                            <option value="Kabupaten Karawang">Kabupaten Karawang</option>
                                                            <option value="Kabupaten Purwakarta">Kabupaten Purwakarta</option>
                                                            //4
                                                            <option value="Kabupaten Garut">Kabupaten Garut</option>
                                                            <!--
                                                            <option value="Kabupaten Adm. Kep. Seribu">Kabupaten Adm. Kep. Seribu</option>
                                                            <option value="Kota Adm. Jakarta Pusat">Kota Adm. Jakarta Pusat</option>
                                                            <option value="Kota Adm. Jakarta Utara">Kota Adm. Jakarta Utara</option>
                                                            <option value="Kota Adm. Jakarta Barat">Kota Adm. Jakarta Barat</option>
                                                            <option value="Kota Adm. Jakarta Selatan">Kota Adm. Jakarta Selatan</option>
                                                            <option value="Kota Adm. Jakarta Timur">Kota Adm. Jakarta Timur</option>
                                                            <option value="Kota Tangerang">Kota Tangerang</option>
                                                            <option value="Kota Tangerang Selatan">Kota Tangerang Selatan</option>
                                                            <option value="Kabupaten Tangerang">Kabupaten Tangerang</option>
                                                            -->
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Kecamatan
                                                        </label>
                                                        <select class="form-control-sm form-control" name="kecamatan" id="2_kecamatan" required>
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT kecamatan FROM kecamatan where kabupaten='Kota Bandung'");
                                                            while($data_per=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_per["kecamatan"] ?>">
                                                                <?php echo $data_per["kecamatan"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Tipe Mobil
                                                        </label>

                                                        <select name="tipe" id="2_tipe"
                                                            class="form-control-sm form-control">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct tipe FROM detail_mobil where jenis_mobil='$data2[jenis_mobil]' order by id_detail_mobil='$data2[id_detail_mobil]' desc");
                                                            while($data_per=mysqli_fetch_array($q)){
        
                                                            ?>
                                                            <option value="<?php echo $data_per["tipe"] ?>">
                                                                <?php echo $data_per["tipe"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Harga
                                                        </label>

                                                        <select id="2_harga" name="harga"
                                                            class="form-control-sm form-control" readonly>
                                                            <option
                                                                value="<?php echo "Rp. ".number_format($harga['harga']) ?>">
                                                                <?php echo "Rp ".number_format($harga['harga'])?>
                                                            </option>
                                                        </select>
                                                    </div>

                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=cash_perorangan">
                                                            Perorangan
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=cash_perusahaan">
                                                            Badan Usaha
                                                        </button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="formbayar">
                                            <div class="" id="headingThree">
                                                <h2 class="mb-0">

                                                    <button class="btn btn-block btn-info btn-lg " type="button"
                                                        data-toggle="collapse" data-target="#collapseThree"
                                                        aria-expanded="true" aria-controls="collapseThree">

                                                        Ajukan Trade In
                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                                data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <form method="post">
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control-sm form-control"
                                                            name="merk" id="3_merk"
                                                            value="<?php echo $data2['jenis_mobil']; ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Lokasi
                                                        </label>
                                                        <select name="kabupaten" id="kabupaten"
                                                            class="form-control-sm form-control">
                                                            //1
                                                            <option value="Kota Bandung">Kota Bandung</option>
                                                            <option value="Kabupaten Bandung">Kabupaten Bandung</option>
                                                            <option value="Kabupaten Bandung Barat">Kabupaten Bandung Barat</option>
                                                            <option value="Kota Cimahi">Kota Cimahi</option>
                                                            <!--
                                                            <option value="Kabupaten Tasikmalaya">Kabupaten Tasikmalaya</option>
                                                            <option value="Kota Tasikmalaya">Kota Tasikmalaya</option>
                                                            <option value="Kabupaten Sumedang">Kota Sumedang</option>
                                                            -->
                                                            //2
                                                            <option value="Kabupaten Majalengka">Kabupaten Majalengka</option>
                                                            <option value="Kabupaten Indramayu">Kabupaten Indramayu</option>
                                                            <option value="Kabupaten Kuningan">Kabupaten Kuningan</option>
                                                            <option value="Kabupaten Cirebon">Kabupaten Cirebon</option>
                                                            <option value="Kota Cirebon">Kota Cirebon</option>
                                                            //3
                                                            <option value="Kabupaten Subang">Kabupaten Subang</option>
                                                            <option value="Kabupaten Karawang">Kabupaten Karawang</option>
                                                            <option value="Kabupaten Purwakarta">Kabupaten Purwakarta</option>
                                                            //4
                                                            <option value="Kabupaten Garut">Kabupaten Garut</option>
                                                            <!--
                                                            <option value="Kabupaten Adm. Kep. Seribu">Kabupaten Adm. Kep. Seribu</option>
                                                            <option value="Kota Adm. Jakarta Pusat">Kota Adm. Jakarta Pusat</option>
                                                            <option value="Kota Adm. Jakarta Utara">Kota Adm. Jakarta Utara</option>
                                                            <option value="Kota Adm. Jakarta Barat">Kota Adm. Jakarta Barat</option>
                                                            <option value="Kota Adm. Jakarta Selatan">Kota Adm. Jakarta Selatan</option>
                                                            <option value="Kota Adm. Jakarta Timur">Kota Adm. Jakarta Timur</option>
                                                            <option value="Kota Tangerang">Kota Tangerang</option>
                                                            <option value="Kota Tangerang Selatan">Kota Tangerang Selatan</option>
                                                            <option value="Kabupaten Tangerang">Kabupaten Tangerang</option>
                                                            -->
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Kecamatan
                                                        </label>
                                                        <select class="form-control-sm form-control" name="kecamatan" id="3_kecamatan" required>
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT kecamatan FROM kecamatan where kabupaten='Kota Bandung'");
                                                            while($data_per=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_per["kecamatan"] ?>">
                                                                <?php echo $data_per["kecamatan"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Tipe Mobil
                                                        </label>

                                                        <select name="tipe" id="3_tipe"
                                                            class="form-control-sm form-control">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct tipe FROM detail_mobil where jenis_mobil='$data2[jenis_mobil]' order by id_detail_mobil='$data2[id_detail_mobil]' desc");
                                                            while($data_per=mysqli_fetch_array($q)){
        
                                                            ?>
                                                            <option value="<?php echo $data_per["tipe"] ?>">
                                                                <?php echo $data_per["tipe"] ?></option>

                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1">
                                                            Harga
                                                        </label>

                                                        <select id="3_harga" name="harga"
                                                            class="form-control-sm form-control" readonly>
                                                            <option
                                                                value="<?php echo "Rp. ".number_format($harga['harga']) ?>">
                                                                <?php echo "Rp ".number_format($harga['harga'])?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=tradein_perorangan">
                                                            Perorangan
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <button type="submit" class="btn btn-block btn-primary btn-lg" formaction="?tampil=tradein_perusahaan">
                                                            Badan Usaha
                                                        </button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Testimoni By SiAxing Start-->
                    <div class="col-md-12">
                        <br><br>
                        
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <h6><i>Testimoni</i></h6>
                            <div class="carousel-item active">
                                <div class="col-md-8">
                                    <div class="card">
                                        <?php
                                        $testi = mysqli_query ($koneksi, "SELECT * FROM testimoni where id_testimoni='8'");
                                        $testimoni_1 = mysqli_fetch_array ($testi);
                                        ?>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img class="d-block w-100" src="<?php echo $testimoni_1['gambar']; ?>" alt="First   slide">
                                                </div>
                                                <div class="col-md-8">
                                                    <br>
                                                    <h6><?php echo $testimoni_1['nama_konsumen']; ?></h6>
                                                    <p><i>(<?php echo $testimoni_1['alamat']; ?>)</i></p>
                                                    <p class="text-dark"><?php echo $testimoni_1['isi']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $testi_2 = mysqli_query ($koneksi, "SELECT nama_konsumen, gambar, alamat, isi FROM testimoni where id_testimoni not in ('8') order by isi desc limit 3");
                                    while ($testimoni_2 = mysqli_fetch_array ($testi_2)){
                            ?>
                            <div class="carousel-item">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img class="d-block w-100" src="<?php echo $testimoni_2['gambar']; ?>" alt="">
                                                </div>
                                                <div class="col-md-8">
                                                    <br>
                                                    <h6><?php echo $testimoni_2['nama_konsumen']; ?></h6>
                                                    <p><i>(<?php echo $testimoni_2['alamat']; ?>)</i></p>
                                                    <p class="text-dark"><?php echo $testimoni_2['isi']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                    }
                            ?>
                        </div>
                        </div>
                    </div>
                    <!-- Testimoni By SiAxing End -->
                </div>
                <!-- Komentar FB -->
                <br><br>
                <div class="fb-comments" data-href="https://suzukipriangan.com/docs/plugins/comments#configurator" data-width="700" data-numposts="5"></div>
                <!-- Komentar FB End -->
            </div>

        </div>
                </div>
            </div>
            <!-- Modal Perorangan Start -->
    <!-- modalnya mengungsi sebentar -->
    <!-- Modal Trade In Perusahaan End -->
        </section>
<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick/slickSlider.js"></script>

<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#2_kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#2_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#2_kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#3_kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#3_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#3_kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<!-- Ganti Kecamatan END -->
<script type="text/javascript">
    $("#3_tipe").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#3_tipe").val();
        var kabupaten = $("#3_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_tipe_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#3_harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#3_kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#3_tipe").val();
        var kabupaten = $("#3_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_regional_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#3_harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#2_kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#2_tipe").val();
        var kabupaten = $("#2_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_regional_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#2_harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#2_tipe").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#2_tipe").val();
        var kabupaten = $("#2_kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_tipe_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#2_harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_regional_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_regional_change_dp.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#dp").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_regional_change_angsuran.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#angsuran").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#tipe").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_tipe_change_harga.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#harga").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#tipe").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_tipe_change_dp.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#dp").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#tipe").change(function(){
        
        // variabel dari nilai combo box unit
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_tipe_change_angsuran.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#angsuran").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
    
    $("#dp").change(function(){
        
        // variabel dari nilai combo box unit
        var dp = $("#dp").val();
        var tipe = $("#tipe").val();
        var kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "public/detail_mobil/ajax_dp_change_angsuran.php",
            data: "kabupaten="+kabupaten+"&tipe="+tipe+"&dp="+dp,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Harga');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#angsuran").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $('#submitBtn1').click(function () {
        $('#merk1').val($('#merk').val());
        $('#tipe1').val($('#tipe').val());
        $('#dp1').val($('#dp').val());
        $('#angsuran1').val($('#angsuran').val());
        $('#kabupaten1').val($('#kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>
<script type="text/javascript">
    $('#submitBtn2').click(function () {
        $('#merk2').val($('#merk').val());
        $('#tipe2').val($('#tipe').val());
        $('#dp2').val($('#dp').val());
        $('#angsuran2').val($('#angsuran').val());
        $('#kabupaten2').val($('#kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>
<script type="text/javascript">
    $('#submitBtn3').click(function () {
        $('#2_tipe1').val($('#2_tipe').val());
        $('#2_harga1').val($('#2_harga').val());
        $('#2_merk1').val($('#2_merk').val());
        $('#2_kabupaten1').val($('#2_kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>
<script type="text/javascript">
    $('#submitBtn4').click(function () {
        $('#2_tipe2').val($('#2_tipe').val());
        $('#2_harga2').val($('#2_harga').val());
        $('#2_merk2').val($('#2_merk').val());
        $('#2_kabupaten2').val($('#2_kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>
<script type="text/javascript">
    $('#submitBtn5').click(function () {
        $('#3_tipe1').val($('#3_tipe').val());
        $('#3_harga1').val($('#3_harga').val());
        $('#3_merk1').val($('#3_merk').val());
        $('#3_kabupaten1').val($('#3_kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>
<script type="text/javascript">
    $('#submitBtn6').click(function () {
        $('#3_tipe2').val($('#3_tipe').val());
        $('#3_harga2').val($('#3_harga').val());
        $('#3_merk2').val($('#3_merk').val());
        $('#3_kabupaten2').val($('#3_kabupaten').val());
    });

    $('#submit').click(function () {
        alert('submitting');
        $('#formfield').submit();
    });
</script>