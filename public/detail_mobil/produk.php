    <?php

        include ("lib/koneksi.php");

    ?>    
            <section class="htc__product__area shop__page ptb--50 bg__white">
                <div class="container">
                    <div class="htc__product__container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section__title section__title--2 text-center">
                                    <h2 class="title__line">Mobil</h2>
                                </div>
                            </div>
                        </div>
                        <!-- End Product MEnu -->
                        <div class="row">
                            <div class="product__list another-product-style">
                                <!-- Start Single Product -->
                                <?php

                                $sql = mysqli_query ($koneksi, "select detail_mobil.jenis_mobil, detail_mobil.spesifikasi, detail_mobil.link, detail_mobil.id_detail_mobil, detail_mobil.img, dp.harga from detail_mobil, dp where detail_mobil.tipe = dp.tipe group by jenis_mobil;");
                                while ($data = mysqli_fetch_array ($sql)){

                                ?>
                                <div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-4 col-xs-12">
                                    <div class="product foo">
                                        <div class="product__inner">
                                            <div class="pro__thumb">
                                                <a href="#">
                                                    <img src="img/mobil/<?php echo $data['img'];?>" alt="product images">
                                                </a>
                                            </div>
                                            <div class="product__hover__info">
                                                <ul class="product__action">
                                                    <li><a title="Shop Now" href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>"><span class="ti-shopping-cart"></span></a></li>
                                                    <li><a title="Unduh Brosur" href="img/spesifikasi/<?php echo $data['spesifikasi']; ?>"><span class="ti-import"></span></a></li>
                                                    <li><a title="Lihat Spesifikasi" href="<?php echo $data['link']; ?>" target="_blank"><span class="ti-book"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product__details">
                                            <h2><a href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>"><?php echo ucwords($data['jenis_mobil']);?></a></h2>
                                            <ul class="product__price">
                                                Mulai Dari
                                                <li class="new__price"><?php echo "Rp ".number_format($data['harga']);?></li>
                                            </ul>
                                            <div class="btn-group">
                                                <a href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>" class="btn btn-md btn-outline-secondary">
                                                    <i class="fa fa-shopping-basket"></i> Shop
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="img/spesifikasi/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                                    <i class="fa fa-info"></i> Detail
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                                <!-- End Single Product -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>