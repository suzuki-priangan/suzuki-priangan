        <section class="htc__product__area shop__page ptb--50 bg__white">
            <div class="container">
                <div class="htc__product__container">
                    <?php
                    if (!defined ("INDEX")) {
                    ?>
                    <section class="htc__store__area ptb--10 bg__white">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section__title section__title--2 text-center">
                                        <p>Maaf, untuk dapat mengakses halaman ini, Anda diharuskan melakukan login terlebih dahulu.</p>
                                        <p class="mb-0">Atau silahkan melakukan registrasi jika Anda belum mempunyai akun.</p>
                                    </div>
                                    <div class="store__btn">
                                        <a href="?tampil=login_form">login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
                        
                        }else{
                        
                        include ("lib/koneksi.php");
                    ?>
<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Booking Service</h2>
                </div>
            </div>
        </div>
        <div class="checkout-form">
            <div class="checkout-form-inner">
               <form action="?tampil=service_proses" method="POST">

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputNama4">Nama*</label>
                        <input type="text" class="form-control" id="inputNama4" placeholder="Nama" name="nama">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputPassword4">Email*</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email">
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputTelephon4">No. Hp</label>
                        <input type="text" class="form-control" id="inputTelephon4" placeholder="No. Handphone" name="hp">
                     </div>
                     <div class="form-group col-md-6">
                     <label for="inputTelephon4">Whatsapp</label>
                        <input type="text" class="form-control" id="inputTelephon4" placeholder="No. Whatsapp" name="wa">
                     </div>
                  </div>
                  
                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputKendaraan4">Pilih Kendaraan*</label>
                        <select id="merk_mobil" class="form-control" name="merk">
                            <option>Pilih kendaraan Anda</option>
                            <?php
                            $q=mysqli_query($koneksi, "SELECT distinct merk_mobil FROM mobil");
                            while($data_merk=mysqli_fetch_array($q)){
        
                            ?>
                            <option value="<?php echo $data_merk["merk_mobil"] ?>"><?php echo $data_merk["merk_mobil"] ?></option>
    
                            <?php
                            }
                            ?>
                        </select>
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputTipe4">Tipe Kendaraan*</label>
                        <select id="tipe" class="form-control" name="tipe">
                        </select>
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputVIN4">Nomor VIN*</label>
                        <input type="text" class="form-control" id="inputVIN4" placeholder="15 - 18 Digit nomor rangka kendaraan" name="no_vin">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputPolisi4">Nomor Polisi*</label>
                        <input type="text" class="form-control" id="inputPolisi4" placeholder="" name="no_pol">
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputSTNK">Nama Pada STNK*</label>
                        <input type="text" class="form-control" id="inputSTNK" name="nama_stnk">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputLayanan">Layanan*</label>
                        <select id="inputLayanan" class="form-control" name="layanan">
                           <?php
                           $layanan = array(
                              "Free Service 1 (1.000 KM)","Free Service 2 (5.000 KM)","Free Service 3 (10.000 KM)","Free Service 4 (20.000 KM)","Free Service 5 (30.000 KM)","Free Service 6 (40.000 KM)","Free Service 7 (50.000 KM)",
                              "Paket A (Kelipatan 5.000 KM)","Paket B (Kelipatan 10.000 KM)","Paket C (Kelipatan 20.000 KM)", "Paket D (Kelipatan 40.000 KM)", "Suzuki Product Quality Update", "Custom Service"
                           );
                           foreach ($layanan as $value) {
                              echo "<option value=".$value.">".$value."</option>";
                           }
                           ?>
                        </select>
                     </div>
                  </div>

                  <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="exampleFormControlSelect1">Kabupaten</label>
                          <select class="form-control" id="kabupaten" name="kabupaten">
                              <?php
                              $q=mysqli_query($koneksi, "SELECT distinct kabupaten FROM kabupaten order by kabupaten");
                              while($data_kab=mysqli_fetch_array($q)){

                              ?>
                              <option value="<?php echo $data_kab["kabupaten"] ?>"><?php echo $data_kab["kabupaten"] ?></option>
    
                              <?php
                              }
                              ?>
                          </select>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="exampleFormControlSelect1">Kecamatan</label>
                          <select class="form-control" id="kecamatan" name="kecamatan">
                          </select>
                      </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputTanggal">Tanggal Service*</label>
                        <div class="">
                        <input type="date" class="form-control" id="inputTanggal" name="tgl">
                        </div>
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputJam">Jam Service*</label>
                        <select id="inputJam" class="form-control" name="jam_service">
                            <?php
                            $i = 8;
                            while($i <= 21){
                                $jam = $i.":00";
                                echo "<option value=".$jam.">".$jam."</option>";
                                $i++;
                            }
                            ?>
                        </select>
                     </div>
                  </div>
                   <div class="form-group col-md-6">
                  <button type="submit" class="btn btn-info" name="booking_service">Daftar Service</button>
                   </div>
               </form>
                   </div>
        </div>
    </div>
</section>
    <section class="htc__blog__area bg__white pb--130">
        <div class="container">
    </div>
</section>
<?php
    }
?>

<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        
        // variabel dari nilai combo box unit
        var id_provinsi = $("#provinsi").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kabupaten.php",
            data: "provinsi_id="+id_provinsi,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kabupaten');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kabupaten").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#merk_mobil").change(function(){
        
        // variabel dari nilai combo box unit
        var id_merk = $("#merk_mobil").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/tipe.php",
            data: "merk_id="+id_merk,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada tipe mobil');
                }
                
                // jika dapat mengambil data,, tampilkan di combo box
                else{
                    $("#tipe").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>