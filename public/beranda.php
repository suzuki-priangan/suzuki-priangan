    <?php

        include ("lib/koneksi.php");

    ?>        
    <!-- Start Feature Product -->
        <section class="categories-slider-area bg__white">
            <div class="container">
                <div class="row">
                    <!-- Start Left Feature -->
                    <div class="col-md-12 col-lg-12 col-sm-8 col-xs-12 float-left-style">
                        <!-- Start Slider Area -->
                        <div class="slider__container slider--one">
                            <div class="slider__activation__wrap owl-carousel owl-theme">
                                <!-- Start Single Slide -->
                                <div class="slide slider__full--screen slider-height-inherit slider-text-left" style="background: rgba(0, 0, 0, 0) url(img/beranda/Gambar_1.jpg) no-repeat scroll center center / cover ;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                                                <div class="slider__inner">
                                                    <h1>Kredit Mobil</h1>
                                                    <h1><font class="text--theme"> Harga Terjangkau</font></h1>
                                                    <div class="slider__btn">
                                                        <h2><a href="?tampil=mobil">SHOP NOW --></a></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Slide -->
                                <!-- Start Single Slide -->
                                <!-- Slider kedua-->
                                <!-- End Single Slide -->
                            </div>
                        </div>
                        <!-- Start Slider Area -->
                    </div>
                    
                    <!-- End Left Feature -->
                </div>
            </div>
        </section>
        <!-- ? -->
        <section class="htc__blog__area bg__white pb--5">
            <div class="container">
                <div class="row">
                    <div class="blog__wrap clearfix mt--60 xmt-10">
                        <!-- Start Single Blog -->
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="?tampil=shop">
                                            <img src="img/feature/features-web-shop.png" alt="blog images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Blog -->
                        <!-- Start Single Blog -->
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="?tampil=service">
                                            <img src="img/feature/features-web-service.png" alt="blog images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Blog -->
                        <!-- Start Single Blog -->
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="#">
                                            <img src="img/feature/features-web-forum.png" alt="blog images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="#">
                                            <img src="img/feature/features-web-hotdeals.png" alt="blog images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Blog -->
                    </div>
                </div>
            </div>
        </section>
        <section class="htc__blog__area bg__white pb--10">
            <div class="container">
                <div class="row">
                    <div class="blog__wrap clearfix mt--60 xmt-10">
                        <!-- Start Single Blog -->
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <img src="img/iklan-promo/whyus.jpg" alt="blog images">
                                    </div>
                                    <div class="blog__hover__info">
                                        <div class="blog__hover__action">
                                            <div class="blog__btn">
                                                <a class="read__more__btn" href="?tampil=whyus">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Blog -->
                        <!-- Start Single Blog -->
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <img src="img/iklan-promo/carakredit.jpg" alt="blog images">
                                    </div>
                                    <div class="blog__hover__info">
                                        <div class="blog__hover__action">
                                            <div class="blog__btn">
                                                <a class="read__more__btn" href="?tampil=carakredit">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Blog -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ? End -->
        <!-- End Feature Product -->
        <div class="only-banner ptb--50 bg__white">
            <div class="container">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="only-banner-img">
                                <a href="#"><img src="img/beranda/slider/Artboard_1.jpg" alt="new product"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Our Product Area -->
        <section class="htc__product__area bg__white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section__title section__title--2 text-center">
                            <h2 class="title__line">Rekomendasi Kami</h2>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-style-tab">
                            <div class="product-tab-list">
                                <!-- Nav tabs -->
                                <ul class="tab-style" role="tablist">
                                    <li class="active">
                                        <a href="#home1" data-toggle="tab">
                                            <div class="tab-menu-text">
                                                <h4>Mobil</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#home2" data-toggle="tab">
                                            <div class="tab-menu-text">
                                                <h4>Aksesoris</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#home3" data-toggle="tab">
                                            <div class="tab-menu-text">
                                                <h4>Karoseri</h4>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content another-product-style jump">
                                <div class="tab-pane active" id="home1">
                                    <div class="row">
                                        <div class="product-slider-active owl-carousel">
                                            <?php

                                            $sql = mysqli_query ($koneksi, "select detail_mobil.jenis_mobil, detail_mobil.spesifikasi, detail_mobil.link, detail_mobil.id_detail_mobil, detail_mobil.img, dp.harga from detail_mobil,   dp where detail_mobil.tipe = dp.tipe group by jenis_mobil order by rand() limit 6;");
                                            while ($data = mysqli_fetch_array ($sql)){

                                            ?>
                                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                                <div class="product">
                                                    <div class="product__inner">
                                                        <div class="pro__thumb">
                                                            <a href="#">
                                                                <img src="img/mobil/<?php echo $data['img'];?>" alt="product images">
                                                            </a>
                                                        </div>
                                                        <div class="product__hover__info">
                                                            <ul class="product__action">
                                                                <li><a title="Shop Now" href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>"><span class="ti-shopping-cart"></span></a></li>
                                                                <li><a title="Unduh Brosur" href="img/spesifikasi/<?php echo $data['spesifikasi']; ?>"><span class="ti-import"></span></a></li>
                                                                <li><a title="Lihat Spesifikasi" href="<?php echo $data['link']; ?>" target="_blank"><span class="ti-book"></span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="product__details">
                                                        <h2><a href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>"><?php echo ucwords($data['jenis_mobil']);?></a></h2>
                                                        <ul class="product__price">
                                                            Mulai Dari
                                                            <li class="new__price"><?php echo "Rp ".number_format($data['harga']);?></li>
                                                        </ul>
                                                        <div class="btn-group">
                                                            <a href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>" class="btn btn-md btn-outline-secondary">
                                                                <i class="fa fa-shopping-basket"></i> Shop
                                                            </a>
                                                        </div>
                                                        <div class="btn-group">
                                                            <a href="img/spesifikasi/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                                    <i class="fa fa-info"></i> Detail
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="home2">
                                    <div class="row">
                                        <div class="product-slider-active owl-carousel">
                                            <?php
            
                                            $sql = mysqli_query ($koneksi, "SELECT harga_acc, merk_acc, nama_acc, gambar_acc, id_aksesoris FROM aksesoris order by rand() limit 6");
                                            while ($data = mysqli_fetch_array ($sql)){
                                
                                            ?>
                                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                                <div class="product">
                                                    <div class="product__inner">
                                                        <div class="pro__thumb">
                                                            <a href="#">
                                                                <img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" alt="product images">
                                                            </a>
                                                        </div>
                                                        <div class="product__hover__info">
                                                            <ul class="product__action">
                                                                <li><a title="Shop Now" href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>"><span class="ti-shopping-cart"></span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="product__details">
                                                        <h2><a href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>"><?php echo $data['nama_acc'];?></a></h2>
                                                        <ul class="product__price">
                                                            <li class="new__price"><?php echo "Rp ".number_format($data['harga_acc']);?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="home3">
                                    <div class="row">
                                        <div class="product-slider-active owl-carousel">
                                            <?php

                                            $sql = mysqli_query ($koneksi, "select jenis_karoseri, harga, img, kelas, id_karoseri from karoseri order by rand() limit 6");
                                            while ($data = mysqli_fetch_array ($sql)){

                                            ?>
                                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                                <div class="product">
                                                    <div class="product__inner">
                                                        <div class="pro__thumb">
                                                            <a href="#">
                                                                <img src="img/karoseri/<?php echo $data['img'];?>" alt="product images">
                                                            </a>
                                                        </div>
                                                        <div class="product__hover__info">
                                                            <ul class="product__action">
                                                                <li><a title="Shop Now" href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>"><span class="ti-shopping-cart"></span></a></li>
                                                                <li><a title="Unduh Brosur" href="img/karoseri/<?php echo $data['spesifikasi']; ?>"><span class="ti-import"></span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="product__details">
                                                        <h2><a href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>"><?php echo ucwords($data['jenis_karoseri']);?></a></h2>
                                                        <ul class="product__price">
                                                            Kelas <strong><?php echo $data['kelas']; ?></strong>
                                                        </ul>
                                                        <ul class="product__price">
                                                            <li class="new__price"><?php echo "Rp ".number_format($data['harga']);?></li>
                                                        </ul>
                                                        <div class="btn-group">
                                                            <a href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>" class="btn btn-md btn-outline-secondary">
                                                                <i class="fa fa-shopping-basket"></i> Shop
                                                            </a>
                                                        </div>
                                                        <div class="btn-group">
                                                            <a href="img/karoseri/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                                                <i class="fa fa-info"></i> Detail
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Our Product Area -->
        
        <!-- Start Blog Area -->
        <section class="htc__blog__area bg__white pb--130">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section__title section__title--2 text-center">
                            <br><br><br>
                            <h2 class="title__line">Artikel & Tips Terbaru</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="blog__wrap clearfix mt--60 xmt-30">
                        <!-- Start Single Blog -->
                        <?php
                        
                        $batas = 100;
                        $sql = mysqli_query ($koneksi, "SELECT judul, isi, id_artikel, tgl_tulis, img_artikel FROM artikel limit 3");
                        while ($data = mysqli_fetch_array ($sql)){

                        ?>
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="blog-details.html">
                                            <img src="img/artikel/<?php echo $data['img_artikel']; ?>" alt="blog images">
                                        </a>
                                        <div class="blog__post__time">
                                            <div class="post__time--inner">
                                                <span class="month"><?php echo $data['tgl_tulis']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog__hover__info">
                                        <div class="blog__hover__action">
                                            <p class="blog__des"><a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>"><?php echo substr($data['isi'], 0, $batas) . '...'; ?></a></p>
                                            <ul class="bl__meta">
                                                <li>By :<a href="#">Admin</a></li>
                                            </ul>
                                            <div class="blog__btn">
                                                <a class="read__more__btn" href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <!-- End Single Blog -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End Blog Area -->

    <!-- jquery latest version -->
    <script src="js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="js/bootstrap/lama/bootstrap.min.js"></script>