<?php

    include ("lib/koneksi.php");

?>    
        <section class="htc__product__area shop__page ptb--50 bg__white">
            <div class="container">
                <div class="htc__product__container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <h2 class="title__line">Shop</h2>
                            </div>
                        </div>
                    </div>
                    <!-- End Product MEnu -->
                    <div class="row">
                        <div class="product__list another-product-style">
                            <!-- Start Single Product -->
                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="?tampil=mobil">
                                                <img src="img/core-img/bg-shop-mobil.png" alt="product images">
                                            </a>
                                            <h2 class="text-center">Mobil</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="?tampil=aksesoris_kategori">
                                                <img src="img/core-img/bg-shop-aksesoris.png" alt="product images">
                                                <h2 class="text-center">Aksesoris</h2>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 single__pro col-lg-4 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="#">
                                                <img src="img/core-img/bg-shop-spareparts.png" alt="product images">
                                                <h2 class="text-center">Spareparts</h2>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Product -->
                        </div>
                    </div>
                </div>
            </div>
        </section>