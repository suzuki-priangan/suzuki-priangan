<?php

    include ("lib/koneksi.php");

?>    
<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Artikel</h2>
                </div>
            </div>
        </div>
        <div class="htc__blog__area bg__white ptb--120">
            <div class="container">
                <div class="row">
                    <div class="blog__wrap blog--page clearfix">
                        <!-- Start Single Blog -->
                        <?php
            
                            $sql = mysqli_query ($koneksi, "select judul, id_artikel, img_artikel, penulis, tgl_tulis from artikel order by tgl_tulis desc");
                            while ($data = mysqli_fetch_array ($sql)){
                                
                            ?>
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="blog foo">
                                <div class="blog__inner">
                                    <div class="blog__thumb">
                                        <a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">
                                            <img src="img/artikel/<?php echo $data['img_artikel'];?>" alt="product images">
                                        </a>
                                        <div class="blog__post__time">
                                            <div class="post__time--inner">
                                                <span class="month"><?php echo $data['tgl_tulis']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog__hover__info">
                                        <div class="blog__hover__action">
                                            <p class="blog__des"><a href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">Lorem ipsum dolor sit consectetu.</a></p>
                                            <ul class="bl__meta">
                                                <li>By :<a href="#"> <?php echo $data['penulis']; ?></a></li>
                                            </ul>
                                            <div class="blog__btn">
                                                <a class="read__more__btn" href="?tampil=artikel_detail&id=<?php echo $data['id_artikel']; ?>">read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <!-- End Single Blog -->
                        <div class="row mt--60">
                            <div class="col-md-12">
                                <div class="htc__loadmore__btn">
                                    <a href="#">load more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="htc__blog__area bg__white pb--130">
        <div class="container">
    </div>
</section>