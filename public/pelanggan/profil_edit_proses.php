<?php
    include ("lib/koneksi.php");

    date_default_timezone_set('Asia/Jakarta');

    $temp      = $_FILES['foto_ktp']['tmp_name'];
    $name      = $_FILES['foto_ktp']['name'];
    $nama_baru = date("dmY_h:i:sa-").$name;
    $size      = $_FILES['foto_ktp']['size'];
    $type      = $_FILES['foto_ktp']['type'];
    $folder    = "img/foto_ktp_pelanggan/";

    if($type == "image/jpeg" || $type == "image/jpg" || $type == "image/gif" || $type == "image/png")
    {
        $delete_file = "img/foto_ktp_pelanggan/$konsumen[foto_ktp]";
        $files = glob($delete_file);
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }
        
        move_uploaded_file($temp, $folder.$nama_baru);
        
        mysqli_query ($koneksi, "UPDATE konsumen SET
        no_ktp = '$_POST[no_ktp]',
        foto_ktp = '$nama_baru',
        nama_konsumen = '$_POST[nama]',
        jenis_kelamin = '$_POST[jenis_kelamin]',
        tgl_lahir = '$_POST[tanggal_lahir]'
        WHERE id_konsumen=$konsumen[id_konsumen]") or die ("Gagal melakukan pesanan");

        echo "<meta http-equiv='refresh' content='0; url=?tampil=profil'>";
        
    }
    else
    {
        echo "Harap upload gambar dengan Format jpg/jpeggif/png dan Ukuran Max 2MB";
    }
?>