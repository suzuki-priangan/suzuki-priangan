<div class="row">
        <div class="">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="mobil-tab" data-toggle="tab" href="#mobil" role="tab" aria-controls="mobil"
                aria-selected="true">MOBIL</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
                aria-selected="false">AKSESORIS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="sparepart-tab" data-toggle="tab" href="#sparepart" role="tab"
                aria-controls="sparepart" aria-selected="false">SPAREPART</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="karoseri-tab" data-toggle="tab" href="#karoseri" role="tab"
                aria-controls="karoseri" aria-selected="false">KAROSERI</a>
            </li>
          </ul>
          <!-- start mobil -->
          <div class="tab-content mt-4 " id="myTabContent">
            <div class="tab-pane fade show active product-review" id="mobil" role="tabpanel"
              aria-labelledby="mobil-tab">
              <div class="container">

                <!-- start mobil -->
                <div class="row">
                  <div class="col-sm-3">
                    <figure class="figure">
                      <a href="">
                        <img src="img/mobil/Baleno 3.png" class="figure-img img-fluid">
                      </a>
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 4.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>White Pure</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 5.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 6.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                </div>
                <!-- end mobil -->


              </div>
            </div>
            <!-- end -->

            <!-- aksesoris -->
            <div class="tab-pane fade product-review" id="review" role="tabpanel" aria-labelledby="review-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/VLEG.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/SPION.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/BUMPER.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/990J0M66R07-470.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->

            <!-- start sparepart  -->
            <div class="tab-pane fade product-sparepart" id="sparepart" role="tabpanel" aria-labelledby="sparepart-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart1.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart2.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart4.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->

            <!-- start karoseri -->
            <div class="tab-pane fade product-karoseri" id="karoseri" role="tabpanel" aria-labelledby="sparepart-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/karoseri/Carry 3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart2.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart4.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->
          </div>
        </div>
      </div>