<?php

    include ("lib/koneksi.php");

?>    
        <section class="htc__product__area shop__page ptb--50 bg__white">
            <div class="container">
                <div class="htc__product__container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <h2 class="title__line">Aksesoris | <?php echo $_GET['id']; ?></h2>
                                <br>
                            </div>
                        </div>
                    </div>
                    <!-- End Product MEnu -->
                    <div class="row">
                        <div class="product__list another-product-style">
                            <!-- Start Single Product -->
                            <?php
            
                            $sql = mysqli_query ($koneksi, "SELECT harga_acc, merk_acc, nama_acc, gambar_acc, id_aksesoris FROM aksesoris where kategori='$_GET[id]'");
                            while ($data = mysqli_fetch_array ($sql)){
                                
                            ?>
                            <div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-6 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>">
                                                <img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" alt="product images">
                                            </a>
                                        </div>
                                        <div class="product__hover__info">
                                            <ul class="product__action">
                                                <li><a title="Shop Now" href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>"><span class="ti-shopping-cart"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product__details">
                                        <h2><a href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>"><?php echo $data['nama_acc'];?></a></h2>
                                        <ul class="product__price">
                                            <li class="new__price"><?php echo "Rp ".number_format($data['harga_acc']);?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <!-- End Single Product -->
                        </div>
                    </div>
                </div>
            </div>
        </section>