<?php

    include ("lib/koneksi.php");

?>    
        <section class="htc__product__area shop__page ptb--50 bg__white">
            <div class="container">
                <div class="htc__product__container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <h2 class="title__line">Aksesoris</h2>
                                <br>
                            </div>
                        </div>
                    </div>
                    <!-- End Product MEnu -->
                    <div class="row">
                        <div class="product__list another-product-style">
                            <!-- Start Single Product -->
                            <?php
            
                            $sql = mysqli_query ($koneksi, "SELECT kategori, gambar_acc FROM aksesoris group by kategori");
                            while ($data = mysqli_fetch_array ($sql)){
                                
                            ?>
                            <div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="?tampil=aksesoris&id=<?php echo $data['kategori']; ?>">
                                                <img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" alt="product images">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="product__details">
                                        <h2><a href="?tampil=aksesoris&id=<?php echo $data['kategori']; ?>"><?php echo $data['kategori'];?></a></h2>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <!-- End Single Product -->
                        </div>
                    </div>
                </div>
            </div>
        </section>