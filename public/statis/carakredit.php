<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Cara Mengajukan Kredit di Website Suzuki Priangan</h2>
                </div>
            </div>
        </div>


            <div class="row">
                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-01.png" alt="" class="image-fluid mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Untuk mendapatkan pendampingan kredit, klik banner kredit lalu isi dan lengkapi kolom persyaratan yang telah disediakan</p>
                        </div>
                    </div>
                </div>

                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-02.png" alt="" class="mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Sales Kami akan menghubungi untuk mendampingi anda dengan memilih skema kredit sesuai dengan kebutuhan Anda dan mengajukan proses survei berdasarkan jadwal yang telah disepakati</p>
                        </div>
                    </div>
                </div>

                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-03.png" alt="" class="mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Setelah permintaan kredit disetujui, mobil akan dikirimkan ke alamat Anda dan plat mobil akan dikirimkan setelahnya</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ##### Call To Action End ###### -->