<?php

    include ("lib/koneksi.php");

?>    
<section class="htc__product__area shop__page ptb--30 bg__white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title section__title--2 text-center">
                    <h2 class="title__line">Pembelian Karoseri Perorangan</h2>
                </div>
            </div>
        </div>
        <div class="checkout-form">
            <div class="checkout-form-inner">
                <form action="?tampil=karoseri_perorangan_proses" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="JenisMobil">Jenis Karoseri</label>
                            <input type="text" class="form-control" name="merk_mobil" id="JenisMobil" value="<?php echo $_POST['jenis_karoseri']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="TipeMobil">Tipe</label>
                            <input type="text" class="form-control" name="tipe" id="TipeMobil" value="<?php echo $_POST['tipe']; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="DP">Jumlah</label>
                            <input type="text" class="form-control" name="banyak" id="DP" value="<?php echo $_POST['banyak']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Angsuran">Harga</label>
                            <input type="text" class="form-control" name="angsuran" id="Angsuran" value="<?php echo "Rp ".number_format($_POST['harga']) ?>" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="DP">Kelas</label>
                            <input type="text" class="form-control" name="total_dp" id="DP" value="<?php echo $_POST['kelas']; ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="hidden" class="form-control" name="id_karoseri" id="DP" value="<?php echo $_POST['id_karoseri']; ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section__title section__title--2 text-center">
                                <p>Silahkan isi data diri Anda. Kami hanya akan menanyakan data Anda sekali ini saja, dan data Anda aman bersama kami.  </p>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <input type="hidden" class="form-control" name="kabupaten" id="Kabupaten" value="<?php echo $_POST['kabupaten']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Nama">Nama</label>
                            <input type="text" class="form-control" name="nama" id="Nama" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="NoHp">No Hand Phone</label>
                            <input type="text" class="form-control" name="no_hp" id="NoHp" placeholder="No Hand Phone" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" class="form-control-sm form-control">
                                //1
                                <option value="Kota Bandung">Kota Bandung</option>
                                <option value="Kabupaten Bandung">Kabupaten Bandung</option>
                                <option value="Kabupaten Bandung Barat">Kabupaten Bandung Barat</option>
                                <option value="Kota Cimahi">Kota Cimahi</option>
                                <!--
                                <option value="Kabupaten Tasikmalaya">Kabupaten Tasikmalaya</option>
                                <option value="Kota Tasikmalaya">Kota Tasikmalaya</option>
                                <option value="Kabupaten Sumedang">Kota Sumedang</option>
                                -->
                                //2
                                <option value="Kabupaten Majalengka">Kabupaten Majalengka</option>
                                <option value="Kabupaten Indramayu">Kabupaten Indramayu</option>
                                <option value="Kabupaten Kuningan">Kabupaten Kuningan</option>
                                <option value="Kabupaten Cirebon">Kabupaten Cirebon</option>
                                <option value="Kota Cirebon">Kota Cirebon</option>
                                //3
                                <option value="Kabupaten Subang">Kabupaten Subang</option>
                                <option value="Kabupaten Karawang">Kabupaten Karawang</option>
                                <option value="Kabupaten Purwakarta">Kabupaten Purwakarta</option>
                                //4
                                <option value="Kabupaten Garut">Kabupaten Garut</option>
                                <!--
                                <option value="Kabupaten Adm. Kep. Seribu">Kabupaten Adm. Kep. Seribu</option>
                                <option value="Kota Adm. Jakarta Pusat">Kota Adm. Jakarta Pusat</option>
                                <option value="Kota Adm. Jakarta Utara">Kota Adm. Jakarta Utara</option>
                                <option value="Kota Adm. Jakarta Barat">Kota Adm. Jakarta Barat</option>
                                <option value="Kota Adm. Jakarta Selatan">Kota Adm. Jakarta Selatan</option>
                                <option value="Kota Adm. Jakarta Timur">Kota Adm. Jakarta Timur</option>
                                <option value="Kota Tangerang">Kota Tangerang</option>
                                <option value="Kota Tangerang Selatan">Kota Tangerang Selatan</option>
                                <option value="Kabupaten Tangerang">Kabupaten Tangerang</option>
                                -->
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Kecamatan</label>
                            <select class="form-control-sm form-control" name="kecamatan" id="kecamatan" required>
                                <?php
                                $q=mysqli_query($koneksi, "SELECT kecamatan FROM kecamatan where kabupaten='Kota Bandung'");
                                while($data_per=mysqli_fetch_array($q)){
   
                                ?>
                                <option value="<?php echo $data_per["kecamatan"] ?>">
                                    <?php echo $data_per["kecamatan"] ?></option>

                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control" name="email" id="Email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <div class="col-md-12">
                            <input type="checkbox" class="form-check-input" name="ktp" value="Saya akan menyiapkan persyaratan yang dibutuhkan" required>
                            <label class="form-check-label" for="exampleCheck1">Saya akan menyiapkan persyaratan yang dibutuhkan</label>
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <div class="col-md-12">
                            <input type="checkbox" class="form-check-input" name="setuju" value="Saya akan menyiapkan persyaratan yang dibutuhkan" required>
                            <label class="form-check-label" for="exampleCheck1">Saya akan menyiapkan persyaratan yang dibutuhkan</label>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
    <section class="htc__blog__area bg__white pb--130">
        <div class="container">
    </div>
</section>
<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>