<?php

    include ("lib/koneksi.php");
    $tes = mysqli_query ($koneksi, "SELECT * FROM karoseri where id_karoseri='$_GET[id]'");
    $karoseri = mysqli_fetch_array ($tes);

?>
    <section class="htc__product__details pt--30 pb--100 bg__white">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 col-sm-12 col-xs-12">
                        <div class="product__details__container">
                            <div class="product__big__images" style="max-width: 100%;">
                                <div class="portfolio-full-image tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active product-video-position" id="img-tab-1">
                                        <img src="img/karoseri/<?php echo $karoseri['img'];?>" alt="full-image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6 col-sm-12 col-xs-12 smt-30 xmt-30">
                        <div class="htc__product__details__inner">
                            <div class="pro__detl__title">
                                <h2><?php echo $karoseri['jenis_karoseri']; ?></h2>
                            </div>
                            <div class="pro__dtl__rating">
                                <ul class="pro__rating">
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                </ul>
                            </div>
                            <div class="pro__details">
                                <p>Tipe  : <?php echo $karoseri['tipe']; ?></p>
                                <p>Kelas : <?php echo $karoseri['kelas']; ?></p>
                                <p>Tahun : <?php echo $karoseri['thn_produksi']; ?></p>
                            </div>
                            <ul class="pro__dtl__prize">
                                <li><?php echo "Rp ".number_format($karoseri['harga'])?></li>
                            </ul>
                            
                            <div class="product-action-wrap">
                                <div class="product-quantity">
                                    <form method="post" class="form-keranjang" action="?tampil=keranjang_proses">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <input type="hidden" class="form-control" id="id_karoseri" name="id_karoseri" value="<?php echo $karoseri['id_karoseri']; ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="hidden" class="form-control" id="jenis_karoseri" name="jenis_karoseri" value="<?php echo $karoseri['jenis_karoseri']; ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="hidden" class="form-control" id="tipe" name="tipe" value="<?php echo $karoseri['tipe']; ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="hidden" class="form-control" id="kelas" name="kelas" value="<?php echo $karoseri['kelas']; ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="hidden" class="form-control" id="harga" name="harga" value="<?php echo $karoseri['harga']; ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <p><strong>Banyak</strong></p>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="number" class="form-control" id="banyak" name="banyak" value="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block" id="masukkan-keranjang" formaction="?tampil=karoseri_perorangan"><i class="fa fa-cart-plus"></i><span> Beli Untuk Perorangan</span></button>
                                    </div>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block" id="masukkan-keranjang" formaction="?tampil=karoseri_perusahaan"><i class="fa fa-cart-plus"></i><span> Beli Untuk Perusahaan</span></button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="pro__social__share">
                                <h2>Share :</h2>
                                <ul class="pro__soaial__link">
                                    <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Product Details -->
        <!-- Start Product tab -->
        <section class="htc__product__details__tab bg__white pb--120">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <ul class="product__deatils__tab mb--60" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#description" role="tab" data-toggle="tab">Deskripsi</a>
                            </li>
                            <li role="presentation">
                                <a href="#reviews" role="tab" data-toggle="tab">Review</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="product__details__tab__content">
                            <!-- Start Single Content -->
                            <div role="tabpanel" id="description" class="product__tab__content fade in active">
                                <div class="product__description__wrap">
                                    <div class="product__desc">
                                        <h2 class="title__6">Details</h2>
                                        <p>Tipe  : <?php echo $karoseri['tipe']; ?></p>
                                        <p>Kelas : <?php echo $karoseri['kelas']; ?></p>
                                        <p>Tahun : <?php echo $karoseri['thn_produksi']; ?></p>
                                    </div>                                   
                                </div>
                            </div>
                            <!-- End Single Content -->                         
                            <!-- Start Single Content -->
                            <div role="tabpanel" id="reviews" class="product__tab__content fade">
                                <div class="review__address__inner">
                                    <!-- Start Single Review -->
                                    
                                    <!-- End Single Review -->
                                </div>
                                <!-- Start RAting Area -->
                                
                                <!-- End RAting Area -->
                                
                            </div>
                            <!-- End Single Content -->
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <ul class="product__deatils__tab mb--10 kiri">
                            <li role="presentation" class="active">
                                <a href="#">Produk Serupa</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="container">
                    <div class="htc__product__container">
                    <div class="row">
                        <div class="product__list another-product-style">
                            <!-- Start Single Product -->
                            <?php
    
                            $sql = mysqli_query ($koneksi, "select jenis_karoseri, harga, img, kelas, id_karoseri from karoseri where   id_karoseri not in ('$karoseri[id_karoseri]') order by rand() limit 4");
                                    while ($data = mysqli_fetch_array ($sql)){
                                
                            ?>
                            <div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            <a href="#">
                                                <img src="img/karoseri/<?php echo $data['img'];?>" alt="product images">
                                            </a>
                                        </div>
                                        <div class="product__hover__info">
                                            <ul class="product__action">
                                                <li><a title="Lihat Detail" href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>"><span class="ti-eye"></span></a></li>
                                                <li><a title="Unduh Brosur" href="img/karoseri/<?php echo $data['spesifikasi']; ?>"><span class="ti-import"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product__details">
                                        <h2><a href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>"><?php echo ucwords($data['jenis_karoseri']);?></a></h2>
                                        <ul class="product__price">
                                            Kelas <strong><?php echo $data['kelas']; ?></strong>
                                        </ul>
                                        <ul class="product__price">
                                            Mulai Dari
                                            <li class="new__price"><?php echo "Rp ".number_format($data['harga']);?></li>
                                        </ul>
                                        <div class="btn-group">
                                                <a href="?tampil=karoseri_detail&id=<?php echo $data['id_karoseri'];?>" class="btn btn-md btn-outline-secondary">
                                                    <i class="fa fa-shopping-basket"></i> Shop
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="img/karoseri/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                                    <i class="fa fa-info"></i> Detail
                                                </a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <!-- End Single Product -->
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
<script type="text/javascript">
    $('#myDropdown').on('show.bs.dropdown', function () {
  // do something…
})
</script>