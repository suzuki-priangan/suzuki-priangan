        <!-- Start Login Register Area -->
        <div class="htc__login__register bg__white ptb--130" style="background: rgba(0, 0, 0, 0) url(images/bg/5.jpg) no-repeat scroll center center / cover ;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="login__register__menu" role="tablist">
                            <li role="presentation" class="login"><a href="?tampil=login_form">Login</a></li>
                            <li role="presentation" class="register active"><a href="?tampil=register_form">Register</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Start Login Register Content -->
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="htc__login__register__wrap">
                            <!-- Start Single Content -->
                            <div id="register" role="tabpanel" class="single__tabs__panel tab-pane fade in active">
                                <form class="login" method="post" action="?tampil=registrasi_proses">
                                    <input type="text" name="nama_konsumen" placeholder="Nama Lengkap*" required>
                                    <input type="email" name="email" placeholder="Email*" required>
                                    <input type="number" name="phone_number" placeholder="Nomor Hp*" required>
                                    <input id="password" name="password" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Minimal 6 Karakter' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="Password*" required>
                                    <span class="form-control-feedback"></span>
                                    <span class="text-warning"></span>
                                    <input id="password_two" name="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Masukkan Password Yang Sama' : '');" placeholder="Verify Password*" required>
                                    <span class="form-control-feedback"></span>
                                    <span class="text-warning"></span>
                                    <div class="htc__login__btn mt--30">
                                        <button type="submit" class="btn btn-info btn-lg">Registrasi</button>
                                    </div>
                                </form>
                            </div>
                            <!-- End Single Content -->
                        </div>
                    </div>
                </div>
                <!-- End Login Register Content -->
            </div>
        </div>