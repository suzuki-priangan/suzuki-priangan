        <!-- Start Login Register Area -->
        <div class="htc__login__register bg__white ptb--130" style="background: rgba(0, 0, 0, 0) url(images/bg/5.jpg) no-repeat scroll center center / cover ;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="login__register__menu" role="tablist">
                            <li role="presentation" class="login active"><a href="?tampil=login_form">Login</a></li>
                            <li role="presentation" class="register"><a href="?tampil=register_form">Register</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Start Login Register Content -->
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="htc__login__register__wrap">
                            <!-- Start Single Content -->
                            <div id="login" role="tabpanel" class="single__tabs__panel tab-pane fade in active">
                                <form class="login" method="post" action="public/register_login/cek_login.php">
                                    <input type="text" name="email" placeholder="Email" required>
                                    <input type="password" name="password" placeholder="Password*" required>
                                    <div class="htc__login__btn mt--30">
                                        <button type="submit" class="btn btn-info btn-lg" >Login</button>
                                    </div>
                                </form>
                            </div>
                            <!-- End Single Content -->
                        </div>
                    </div>
                </div>
                <!-- End Login Content -->
            </div>
        </div>