<?php

    if (isset ($_GET['tampil'])) $tampil = $_GET['tampil'];
    else $tampil = "beranda";

    if ($tampil == "beranda") include ("public/beranda.php");

    //aksesoris
    elseif ($tampil == "aksesoris") include ("public/aksesoris/aksesoris.php");
    elseif ($tampil == "aksesoris_detail") include ("public/aksesoris/aksesoris_detail.php");
    elseif ($tampil == "aksesoris_kategori") include ("public/aksesoris/aksesoris_kategori.php");

    //artikel & tips trik
    elseif ($tampil == "artikel") include ("public/artikel_tips/artikel.php");
    elseif ($tampil == "artikel_detail") include ("public/artikel_tips/artikel_detail.php");
    elseif ($tampil == "tips_trik") include ("public/artikel_tips/tips_trik.php");
    elseif ($tampil == "tips_trik_detail") include ("public/artikel_tips/tips_trik_detail.php");

    //tes drive
    elseif ($tampil == "test-drive") include ("public/test_drive/test-drive.php");
    elseif ($tampil == "test-drive_proses") include ("public/test_drive/test-drive_proses.php");

    //service
    elseif ($tampil == "service") include ("public/service/service.php");
    elseif ($tampil == "service_proses") include ("public/service/service_proses.php");

    //Mobil
    elseif ($tampil == "mobil_detail") include ("public/detail_mobil/detail_mobil.php");
    elseif ($tampil == "mobil") include ("public/detail_mobil/produk.php");
    elseif ($tampil == "kredit_perorangan") include ("public/detail_mobil/kredit_perorangan.php");
    elseif ($tampil == "kredit_perorangan_proses") include ("public/detail_mobil/kredit_perorangan_proses.php");
    elseif ($tampil == "kredit_perusahaan") include ("public/detail_mobil/kredit_perusahaan.php");
    elseif ($tampil == "kredit_perusahaan_proses") include ("public/detail_mobil/kredit_perusahaan_proses.php");
    elseif ($tampil == "cash_perorangan") include ("public/detail_mobil/cash_perorangan.php");
    elseif ($tampil == "cash_perorangan_proses") include ("public/detail_mobil/cash_perorangan_proses.php");
    elseif ($tampil == "cash_perusahaan") include ("public/detail_mobil/cash_perusahaan.php");
    elseif ($tampil == "cash_perusahaan_proses") include ("public/detail_mobil/cash_perusahaan_proses.php");
    elseif ($tampil == "tradein_perorangan") include ("public/detail_mobil/tradein_perorangan.php");
    elseif ($tampil == "tradein_perorangan_proses") include ("public/detail_mobil/tradein_perorangan_proses.php");
    elseif ($tampil == "tradein_perusahaan") include ("public/detail_mobil/tradein_perusahaan.php");
    elseif ($tampil == "tradein_perusahaan_proses") include ("public/detail_mobil/tradein_perusahaan_proses.php");

    //Karoseri
    elseif ($tampil == "karoseri") include ("public/karoseri/karoseri.php");
    elseif ($tampil == "karoseri_detail") include ("public/karoseri/karoseri_detail.php");
    elseif ($tampil == "karoseri_perorangan") include ("public/karoseri/karoseri_perorangan.php");
    elseif ($tampil == "karoseri_perorangan_proses") include ("public/karoseri/karoseri_perorangan_proses.php");
    elseif ($tampil == "karoseri_perusahaan") include ("public/karoseri/karoseri_perusahaan.php");
    elseif ($tampil == "karoseri_perusahaan_proses") include ("public/karoseri/karoseri_perusahaan_proses.php");

    //Register & Login
    elseif ($tampil == "registrasi_proses") include ("public/register_login/registrasi_proses.php");
    elseif ($tampil == "verifikasi") include ("public/register_login/verif_email.php");
    elseif ($tampil == "cek_login") include ("public/register_login/cek_login.php");
    elseif ($tampil == "logout") include ("public/register_login/logout.php");
    elseif ($tampil == "login_form") include ("public/register_login/login_form.php");
    elseif ($tampil == "register_form") include ("public/register_login/register_form.php");

    //Pelanggan
    elseif ($tampil == "profil") include ("public/pelanggan/profil.php");
    elseif ($tampil == "ganti_pass") include ("public/pelanggan/ganti_pass.php");
    elseif ($tampil == "profil_edit_email") include ("public/pelanggan/profil_edit_email.php");
    elseif ($tampil == "profil_edit_proses") include ("public/pelanggan/profil_edit_proses.php");
    elseif ($tampil == "profil_edit_foto") include ("public/pelanggan/profil_edit_foto.php");
    elseif ($tampil == "alamat") include ("public/pelanggan/alamat.php");
    elseif ($tampil == "alamat_tambah") include ("public/pelanggan/alamat_tambah.php");
    elseif ($tampil == "alamat_tambah_proses") include ("public/pelanggan/alamat_tambah_proses.php");
    elseif ($tampil == "transaksi") include ("public/pelanggan/transaksi.php");
    elseif ($tampil == "status_pengajuan") include ("public/pelanggan/status_pengajuan.php");
    elseif ($tampil == "keranjang_proses") include ("public/pelanggan/keranjang_proses.php");
    elseif ($tampil == "keranjang") include ("public/pelanggan/keranjang.php");
    elseif ($tampil == "keranjang_hapus") include ("public/pelanggan/keranjang_hapus.php");
    elseif ($tampil == "pemesanan_proses") include ("public/pelanggan/pemesanan_proses.php");
    
    //elseif ($tampil == "pengajuan_kredit_org_detail") include ("public/pengajuan_kredit_org_detail.php");
    //elseif ($tampil == "pengajuan_kredit_peru_detail") include ("public/pengajuan_kredit_peru_detail.php");
    //elseif ($tampil == "pengajuan_cash_org_detail") include ("public/pengajuan_cash_org_detail.php");
    //elseif ($tampil == "pengajuan_cash_peru_detail") include ("public/pengajuan_cash_peru_detail.php");

    //promo - event
    elseif ($tampil == "promo") include ("public/promo.php");
    elseif ($tampil == "event") include ("public/event.php");
    elseif ($tampil == "carakredit") include ("public/statis/carakredit.php");
    elseif ($tampil == "whyus") include ("public/statis/whyus.php");
    elseif ($tampil == "shop") include ("public/shop.php");

    //forum
    elseif ($tampil == "forum") include ("public/forum/forum.php");

    //tes
    elseif ($tampil == "modal") include ("public/modal.php");
    elseif ($tampil == "tes") include ("public/tes.php");
    elseif ($tampil == "tes2") include ("public/tes2.php");
    elseif ($tampil == "tes3") include ("public/tes3.php");

    else "Konten tidak ada";
?>