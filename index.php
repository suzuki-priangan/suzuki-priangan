<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SUZUKI Priangan | PT SUZUKI Mobilindo </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="images/logo/title.png">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    

    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Owl Carousel main css -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- User style -->
    <link rel="stylesheet" href="css/custom.css">


    <!-- Modernizr JS -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <style>
    .kiri {
        justify-content: left !important;
    }

    .atas {
        margin-top: 0px !important;
    }
    
    .product__details {
            text-align: center !important;
        }

    </style>
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Body main wrapper start -->
    <div class="wrapper fixed__footer">
        <!-- Start Header Style -->
                        <!-- Start MAinmenu Ares -->
                        <?php include("menu.php"); ?>
                        <!-- End MAinmenu Ares -->
        <!-- End Header Style -->
        
        <div class="body__overlay"></div>
        <!-- Start Offset Wrapper -->
        <div class="offset__wrapper">
            <!-- Start Search Popap -->
            <div class="search__area">
                <div class="container" >
                    <div class="row" >
                        <div class="col-md-12" >
                            <div class="search__inner">
                                <form action="#" method="get">
                                    <input placeholder="Search here... " type="text">
                                    <button type="submit"></button>
                                </form>
                                <div class="search__close__btn">
                                    <span class="search__close__btn_icon"><i class="zmdi zmdi-close"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Search Popap -->
            <!-- Start Offset MEnu -->
            
            <!-- End Offset MEnu -->
            <!-- Start Cart Panel -->
            <div class="shopping__cart">
                <div class="shopping__cart__inner">
                    <div class="offsetmenu__close__btn">
                        <a href="#"><i class="zmdi zmdi-close"></i></a>
                    </div>
                    <h2 class="text-center">Ops!</h2>
                    <ul class="shoping__total">
                    </ul>
                    <h3 class="text-center">Login untuk melihat isi keranjang Anda!</h3>
                    <br><br>
                    <ul class="shopping__btn">
                        <li><a href="?tampil=login_form">Login</a></li>
                        <li class="shp__checkout"><a href="?tampil=register_form">Registrasi</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Cart Panel -->
        </div>
        <!-- End Offset Wrapper -->
        <!-- Konten -->
        <?php include ("konten.php"); ?>
        <!-- Konten End -->
        <!-- Start Footer Area -->
        <footer class="htc__foooter__area gray-bg">
            <div class="container">
                <div class="row">
                    <div class="footer__container clearfix">
                         <!-- Start Single Footer Widget -->
                        <div class="col-md-3 col-lg-3 col-sm-6">
                            <div class="ft__widget">
                                <h2 class="ft__title">Kontak Kami</h2>
                                <div class="footer-address">
                                    <ul>
                                        <li>
                                            <div class="address-icon">
                                                <i class="zmdi zmdi-pin"></i>
                                            </div>
                                            <div class="address-text">
                                                <p>Jl Dr Setiabudhi No.78 pasteur, Sukajadi Bandung</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="address-icon">
                                                <i class="zmdi zmdi-email"></i>
                                            </div>
                                            <div class="address-text">
                                                <a href="#"> marketing@suzukipriangan.com</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="address-icon">
                                                <i class="zmdi zmdi-phone-in-talk"></i>
                                            </div>
                                            <div class="address-text">
                                                <p>Bandung 40141 +62 818-430-490</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <ul class="social__icon">
                                    <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Footer Widget -->
                        <!-- Start Single Footer Widget -->
                        <div class="col-md-3 col-lg-2 col-sm-6 smt-30 xmt-30">
                            <div class="ft__widget">
                                <h2 class="ft__title">Layanan</h2>
                                <ul class="footer-categories">
                                    <li><a href="#">Pembelian</a></li>
                                    <li><a href="?tampil=artikel_detail&id=6">Perawatan</a></li>
                                    <li><a href="?tampil=artikel_detail&id=5">Pendampingan Kredit</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Start Single Footer Widget -->
                        <div class="col-md-3 col-lg-2 col-sm-6 smt-30 xmt-30">
                            <div class="ft__widget">
                                <h2 class="ft__title">Informasi</h2>
                                <ul class="footer-categories">
                                    <li><a href="?tampil=artikel_detail&id=8">Tentang Kami</a></li>
                                    <li><a href="?tampil=artikel_detail&id=4">Cara Kerja</a></li>
                                    <li><a href="#">Kebijakan Privasi</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Start Single Footer Widget -->
<!--
                        <div class="col-md-3 col-lg-3 col-lg-offset-1 col-sm-6 smt-30 xmt-30">
                            <div class="ft__widget">
                                <h2 class="ft__title">Sistem Pembayaran</h2>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-visa.png" alt="">
                                    </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-mastercard.png" alt="">
                                    </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-jcb.png" alt="">
                                    </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-amex.png" alt="">
                                    </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-alfa.png" alt="">
                                    </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-info.png" alt="">
                                    </div>
                                    </div>
                                </div>
                                <br> 
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                                    <div class="news-thumbnail">
                                        <img src="https://moladin.com/images/footer-bank.png" alt="">
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
-->
                        <!-- End Single Footer Widget -->
                    </div>
                </div>
                <!-- Start Copyright Area -->
                <div class="htc__copyright__area">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="copyright__inner">
                                <div class="copyright">
                                    <p>© 2019 <a href="https://www.suzukipriangan.com" target="_blank">SUZUKI Priangan | PT SUZUKI Mobilindo</a>
                                    All Right Reserved.</p>
                                </div>
                                <ul class="footer__menu">
                                    <li><a href="?tampil=beranda">Beranda</a></li>
                                    <li><a href="?tampil=shop">Produk</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Copyright Area -->
            </div>
        </footer>
        <!-- End Footer Area -->
    </div>
    <!-- Body main wrapper end -->
    <!-- QUICKVIEW PRODUCT -->
    <!-- Kapan2 diisi -->
    <!-- END QUICKVIEW PRODUCT -->
    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- jquery latest version -->
    <script src="js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="js/plugins.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- Waypoints.min.js. -->
    <script src="js/waypoints.min.js"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="js/main.js"></script>
    <!-- Slick js -->
    <script src="js/slick/slickSlider.js"></script>
    <!-- Font-Awesome -->
    <script src="js/fa/all.min.js"></script>
    <!-- FB JS -->
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>

</body>

</html>